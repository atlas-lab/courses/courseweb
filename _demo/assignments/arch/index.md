---
layout: page
title: Architecture
checkpoints:
    - "Aim to complete 1-5 by 11:59pm on"
# due: "11:59pm"
topics:
    - registers
#    - ram
    - arch
    - floats
policy: individual
worksheet: cs240-arch-worksheet.pdf
worksheet_source: https://docs.google.com/document/d/1IpaDrdbnHoPPUygPeN-uXdThf1oDOlCob4TZFEgc714/edit
---

{% assign ec = '<small class="blue_tag">Extra Fun</small>' %}

## Assignment: {{page.title}}

{% include assignment-manifest.html %}

{:.no_toc}
## Contents

- toc
{:toc}

## Exercises

{% include use-worksheet.md %}

### 1. Reconstructing Memories (14 points)

In this set of exercises, you will reuse provided RAMs (random access
memories) to design RAMs of different dimensions without
reimplementing the internals.

#### Provided RAMs

In an *A*&times;*B* RAM, *A* is the number of addressable locations (cells)
in the memory and *B* is the width of each location (cell) in bits.
Recall that a RAM is much like a register file, but the underlying
storage technology is not based on flip-flops.  *Memory addresses* are
analogous to *register numbers* in a register file; *memory locations
(cells)* are analogous to *registers* in a register file.  Unlike the
register file we designed, these RAMs support reading only one
location at a time.

<figure class="right">
<img src="xram.png" style="width: 250px;" />
<figcaption>RAM diagram</figcaption>
</figure>

The provided RAMs have the following form:

- Each RAM has a *Data In* port, a *Data Out* port, an *Address* input
  and a *Write Enable* input.
- Each RAM is always reading from the location given by the current *Address*,
  expressing its contents on *Data Out*.
- Each RAM also stores the value on *Data In* into the location given
  by the current *Address* if the *Write Enable* input carries a 1.
- Each RAM is indivisible.

#### Rules

You will design new RAMs with the same kind of inputs, outputs, and
externally observable behavior, but with different dimensions.  For
each:

- Draw one or more small boxes of the form above, representing the
  provided RAM(s), inside a larger box of the same form, representing
  the RAM you are implementing.
- Connect inputs and outputs of the larger box with those of the
  smaller boxes, possibly adding combinational logic building blocks.
- Do not draw each individual line of a bus (*e.g.*, Address) unless
  necessary to distinguish the endpoints of the individual lines.  Use
  hatch marks and width annotations instead.

#### Exercises

{:.letter_marker}
1. (4 points) Design a 256&times;8 RAM by adding minimal wiring and
   logic to two identical 256&times;4 RAM components.  Label the
   widths (in bits/lines) of the ports *Address*, *Data In*, and *Data
   Out* for each 256&times;4 RAM you use and for the 256&times;4 RAM
   you implement.

2. (10 points) Design a 64K&times;8 RAM by adding minimal wiring and
   logic to a single provided 16K&times;32 RAM.  (K = "kilo" = 1024)
    - Unlike the previous exercise, this provided RAM component has
      the same capacity as the desired final product, though it has
      different dimensions:
      - The provided RAM has fewer addressable memory locations (*A*)
        than the RAM you will design.
      - The provided RAM has a larger addressable unit of data (*B*)
        than the RAM you will design.
    - Be careful with writes: when writing an 8-bit value into the
      provided RAM, you must be sure to change only the relevant 8
      bits of storage.  All other storage bits should hold the same
      values as before the write operation.


### 2. Taking Control (9 points)

In this exercise you will design the Control Unit for the *HW*
microarchitecture we designed in class.  Our design focused on
connecting the *datapath* components (*e.g.*, the register file, ALU,
memory, etc.), but we left the Control Unit as black box.

{::options parse_block_html="true" /}
<figure>

|Instruction|Opcode<sub>3:0</sub>|Reg Write|ALU Op<sub>3:0</sub>|Mem Store|Mem|Branch|
|LW|0000||||||
|...|...|...|...|...|...|...|

{::options parse_block_html="false" /}
<figcaption>Control Unit truth table</figcaption>
</figure>

Give a truth table with 4-bit *HW* instruction opcodes as inputs, plus
one output for each of the control lines, outputs of the Control Unit
that are control inputs to other components.  The table should have
one row for each of the seven instructions (one row for each opcode)
**except JMP** from the first instruction table
[here]({{site.baseurl}}/slides/arch.pdf).)  The control lines we need
are:

- Reg Write (1 bit): controls the write-enable of the register file.
- ALU Op (4 bits): controls the ALU (with the same 4 ALU control lines
  we developed earlier).
  - From left to right: Invert A, Negate B, Op<sub>1</sub>, Op<sub>0</sub>.
- Mem Store (1 bit): controls the write-enable of the data memory.
- Mem (1 bit): controls three multiplexers deciding which input
  to provide for:
    - the register file's write address;
    - the register file's write data; and
    - the ALU's second operand.
- Branch (1 bit, added during class): controls whether to choose the next PC based on a branch target and test

Please note that Mem Store, Mem, and Branch are all **active high** (0 = False, 1 = True).

<figure>
<img src="datapath-with-beq.png" style="width: 700px;" />
<figcaption><em>HW</em> microarchitecture datapath</figcaption>
</figure>


### 3. Jumping into the Unknown (15 points)

{:.alpha}
1. (9 points) Add logic to the *HW* microarchitecture to implement the
   `JMP` instruction, which we did not implement in class.  You will
   add:

    - One new *Jump* control line from the Control Unit, carrying 1 if
      the current instruction is `JMP` and 0 otherwise.
    - One new column in the Control Unit table for the *Jump* control line.
    - One new row in the Control Unit table for the `JMP` opcode.
    - Wiring and logic using the *Jump* control line and the *offset*
      bits from the `JMP` instruction encoding to store the `JMP`
      instructions's target address to the PC if and only if the
      current instruction is `JMP`.  You will need to "cut" one
      existing wire to splice in some new logic.

    `JMP` sets the PC to the absolute instruction address
   given by its argument (a number) multiplied by 2.  For example,
   `JMP 3` sets the PC to 0x6, causing the instruction stored at
   address 0x6 (*i.e.*, the 3rd instruction in the program) to be
   executed next.

2. (6 points) Assume `R2` and `R3` hold input values when the
   following program starts.  `R4` will hold an output when the
   program stops.  The address of each instruction in the instruction
   memory is shown at left.  The (new) `HALT` instruction stops the
   computer.

    {:.roman}
    1. Execute the program assuming `R2` holds 5 and `R3` holds 2.
       What value does `R4` hold when the computer reaches `HALT`?
    2. Try a couple more examples with (small) positive numbers in
       `R2` and `R3`.  What does this code do with `R2` and `R3` to
       compute `R4`?  Answer with one simple line of valid C code of
       the form: `R4 = ...;`. The C could should use variables `R2`,
       `R3`, and `R4` (corresponding to the *HW* registers) and any
       simple C operations that accomplsih the same effect as these
       combined *HW* instructions. C control flow is *not* needed.

~~~ asm
    # R2 and R3 hold program inputs here.
0:  AND R2, R2, R4
2:  AND R3, R3, R5
4:  BEQ R5, R0, 3
6:  SUB R5, R1, R5
8:  ADD R4, R4, R4
A:  JMP 2
C:  HALT # Stops execution.
    # What value is in R4?
~~~

### 4. Instruction Not Missing (12 points)

The *HW* ISA introduced in class lacks an instruction for bitwise
complement.  A logical choice would be `NOT Rs,Rd`, which takes the
bitwise complement of the value in source register `Rs` and stores
into destination register `Rd`.  You will design two implementations
of the proposed `NOT` instruction.

{:.alpha}
1. (4 points) One way to implement `NOT` is to replace any occurence
   of a `NOT` instruction by a sequence of existing *HW* instructions
   that has the same effect as `NOT`.

{% comment %}
   This adds a `NOT`
   instruction to the *assembly language* for *HW* without actually
   modifying the *HW* ISA.
   {% endcomment %}

   Given source register identifier `Rs` and destination register
   identifier `Rd`, write a sequence of one or more *HW* instructions
   that has the same effect as `NOT Rs,Rd`.  The replacement
   instructions may use values from any registers, but must store
   values only in `Rd`.  Instructions may *not* store values in any
   other register or data memory location.  The sequence may use only
   the instructions defined in class. (See the instruction table
   [here]({{site.baseurl}}/slides/arch.pdf).)

    Hint: Think about the relation of bitwise complement and
    arithmetic rules.
2. (4 points) A second way to implement `NOT` is to add a new
   instruction to the ISA, complete with an unique encoding, and to
   extend the microarchitecture to implement it.  First, implement a
   related instruction (`NAND`) in this way.

    {:.roman}
    1. Define a 16-bit encoding of the
       `NAND` instruction as in the instruction table
       [here]({{site.baseurl}}/slides/arch.pdf).  Choose *any* unused
       opcode for the encoding.
    2. Add a row to the Control Unit Table from the previous problem
       to indicate how to control the datapath to implement `NAND`. Do
       not add extra logic to the datapath.
       
3. (4 points) Consider how to implement `NOT` using `NAND`.  Define an
   encoding for the `NOT` instruction directly in the ISA.  Choose the
   encoding carefully to minimize additions to the Control Unit and
   the datapath.  Assuming you have completed (b), an ideal encoding
   requires no new rows or columns in the Control Unit table.

    **Hint:** Think of how to *translate* the `NOT` instruction to a
   `NAND` instruction and use this intuition when designing the bit
   encoding of `NOT`.


{% comment %}

1. -x = ~x + 1 => ~x = -x -1 = 0 - (x + 1)

    `ADD Rs,R1,Rd; SUB R0,Rd,Rd`
2. Encode as `opcode Rs Rs Rd`.  Use *ALU Op* = any of 1000, 1001,
   0100, 0101.  It's not quite possible to do it with the adder.

{% endcomment %}

### 5. Points Affixed or Afloat in a C of Numbers {{ ec }}

{% alert Extra Fun %}

This exercise is optional. It requires reading about [*fixed point*
and *floating point* number
representations]({{site.baseurl}}/topics/#floats), which may not be
covered in class.

{% endalert %}

[^char]: `char` is often used for characters in C, including with literal character values like `'c'`, but it is just a number type.  It is synonymous with `signed char` in most implementations, just a signed 8-bit integer.  `unsigned char` is an unsigned 8-bit integer.

{:.alpha}
1. [3 points]
   The Sea language (an imaginary programming language otherwise
   identical to C) supports a `signed fixed8ths char` type[^char]
   using an 8-bit signed fixed point number system with precision to
   the 1/8 (eighths) place.

    {:.roman}
    1. What are the minimum and maximum numbers representable by a
       `signed fixed8ths char`? Write your answer as a base ten number using fractions or decimals.
    
    2. What are the minimum and maximum numbers representable by a
       `signed fixed32nds char`, which uses 8-bit signed fixed-point
       representation with precision to the 32nds place? Write your answer as a base ten number using fractions or decimals.

    3. <figure class="right">
       <img src="adder.png" width="150px" />
       <figcaption>8-bit two's complement adder.<br />  <em>A</em>, <em>B</em>, and <em>Sum</em> are 8-bit buses.</figcaption>
       </figure>
       We provide a hardware adder design (at right) that supports
       8-bit two's complement addition (*e.g.*, for `signed char`
       numbers).  Draw a logic diagram for a hardware adder
       that supports the `signed fixed8ths char` number system.  You may use the provided two's complement adder as a black box (no
       need to draw the internals) and extend it with whatever wiring
       and logic is necessary.  There is a "trick" here!  Keep it simple.

2. [3 points] Using the
   [6-bit floating-point encoding defined in class]({{site.baseurl}}/slides/floats.pdf),
   give decimal notation of the numbers represented by the following
   floating-point encodings.

    {:.roman}
    1. `110101`
    2. `100001`
    3. `011100`
    4. `000011`
    5. `010010`
    6. `111101`

{% comment %}
2. [8 points] Complete the function `float_less_or_equal` with an
   expression that implements a less-than-or-equal comparison on the
   bit representations of `x` and `y` without comparisons on
   `float`-type expressions.  You may use `<=` and other comparison
   operators on `unsigned`-type expressions only.  Otherwise, there
   are no restrictions on operators.  The (provided) function
   `float_bits` is used to get this bit representation as an
   `unsigned` 32-bit value.  (Since C actually converts
   representations (instead of reinterpreting bits) when casting to or
   from floating point types, this function is difficult to
   implement.)

    You do not need to produce a C file with your code.  Just write
    the return expression as text.

    Your implementation should handle any `x` and `y` of type `float`
    (IEEE single-precision floating point), including denormalized
    values.  Review the notes from class or CSAPP 2.4
    (especially 2.4.2) to for the details of this representation.

~~~ C
// Returns the unsigned value with the same 32-bit
// *representation* as the float argument.
unsigned float_bits(float a);

// Return true if x <= y, false otherwise.
// Assume x != NaN and y != NaN.
// Treat +0.0 and -0.0 as equal.
int float_less_or_equal(float x, float y) {
    unsigned xbits = float_bits(x);
    unsigned ybits = float_bits(y);

    // Get the sign bits.
    unsigned xsign = xbits >> 31;
    unsigned ysign = ybits >> 31;

    // Fill in the blank with an expression that provides the correct
    // return value, based only on xbits, ybits, xsign, and ysign.
    return _____________________________;
}
~~~

**Hint:** the expression likely will not fit comfortably in one line.
  Think of 4 independent conditions under which the `x<=y` relation
  holds.
{% endcomment %}


{% comment %}
### 5. Shift

<figure class="right">
<img src="shifter.png" style="width: 150px;" />
<figcaption>Logical left shifter</figcaption>
</figure>

The *HW* ISA introduced in class did not include an instruction for
logical shift left.  A logical choice would be `LSL Rs,Rt,Rd`, which
takes the value in register `Rs`, shifts it left logically by the
number of bits given in register `Rt`, and stores the result in
register `Rd`.

Unlike `NOT`, general logical shift cannot be implemented easily with
existing components in the *HW* microarchitecture.


The `LSL` instruction will follow the encoding of
arithmetic instructions, with the opcode `1001`.  We will use the
shifter device shown here to implement shifting. It takes a 16-bit
data input and a 4-bit shift amount input, and produces a 16-bit
output that is *data input \<\< shift amount*.

{:.letter_marker}
1. The data input and output are 16 bits to match the word size of our
   architecture.  Why is the shift amount given in 4 bits?
2. Connect the inputs and output of a Left Shifter in our *HW*
   microarchitecture (shown below) to implement the LSL instruction.
   Use additional logic as needed.  Refer to the
   [class material]({{site.baseurl}}/topics/#arch) for instruction
   encodings and other examples in *HW*.
   - If connecting only a subset of lines from a bus, clearly mark
     which lines, using bit ranges as shown in the datapath diagram
     here.
   - Assume that the Control Unit already controls existing parts of
     the datapath properly for an LSL instruction given its opcode.
3. Describe your design briefly (in at most a few sentences):
   - How does your augmented datapath work to provide the shift instruction?
   - Describe what the Control Unit must do differently for an LSL instruction vs. an AND instruction.
   - If you added new control lines, describe how they should be controlled.

{% endcomment %}



{% comment %}
## Problem Ideas

2. Implement other operations: 
3. Execute a program.

   - Get final state of machine, ISA level.
   - Describe which parts of datapath activated at each step.

4. Stuck at zero faults.

{% endcomment %}
