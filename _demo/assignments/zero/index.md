---
layout: page
# due: "11:59pm"
due: Before the second week's lab meeting on
title: "Zero"
topics:
    - plan
    - bits
links:
    - title: About CS 240 (syllabus and policies)
      path: /about/
    - title: C reference
      path: /tools/#c
policy: individual
repo: zero
---

{% assign visit = false %}


## Assignment: {{page.title}}

{% include assignment-manifest.html %}


{% alert Assignment Manifest %}

Each {{site.course}} assignment starts with an ***assignment
manifest*** describing **Due** dates, the honor code **Policy** for
collaboration and submissions, how to find starter **Code**, how to
**Submit** your work, and a list of topics that are good
**Reference**.

**In this assignment, please ignore** the **Code** and **Submit**
lines above until directed explicitly. For this assignment, learning
how to acquire starter code is part of the assignment itself. In
future assignments, they serve as quick reminders of standard steps,
with links to the full documentation.

*(Orange boxes mark key information or warnings.)*

{% endalert %}

{:.no_toc}
## Contents

- toc
{:toc}

## Overview

This assignment is a joint lecture/lab assignment with a few tasks/goals:

1.  [Introduce yourself.](#intros)
2.  [Install and learn how to use general course software tools and
    infrastructure.](#setup)
3.  [Become familiar with course logistics and policies.](#course)
4.  [Use programming tools and practice some concepts with bits.](#code)

Notes:

- This is an [**individual** assignment]({{site.baseurl}}/about/#honor-code), 
  meaning each student will submit their own work. **Critically, each
  student should complete all of the tool and setup steps in their own
  account.** We will rely on this going forward.
- Since this is a [**practice** assignment]({{site.baseurl}}/about/#honor-code),
  you are encouraged to collaborate on ideas or troubleshooting with
  other students or work together, even though you will each submit
  your own work.
- The tools will be new to most of you. It is expected that you may
  need some support in figuring them out.  Please [post questions or
  visit drop-in hours]({{site.baseurl}}/about/#communication) if you
  get stuck.
- Compared to the assignments that will follow, this one involves more
  tool setup and tool learning, less concept thinking, and fewer
  points.
- You can work on the first three tasks in any order. To write/submit
  answers and complete the last part, you must first complete the
  [setup](#setup) tasks.

{:#intros}
## Introduce Yourself

- **ASAP, join the {{site.course}} {{site.forum.name}}** via the invitation
  link sent by email, then introduce yourself to your classmates
  briefly in the
  [{{site.forum.intros.name}}]({{site.forum.intros.url}}).
- **By the end of the second week of classes, meet with the
  instructor** during drop-in hours or an appointment. We will have a
  brief get-to-know-you chat about:
    - name/pronouns
    - questions, excitement, or concerns you have about the course
    - what brings you to CS
    - the place you would most like to visit in a world with no
      restrictions on your travel

{:#setup}
## Set up Accounts and Tools

Please complete these tasks in order to set up your computer and CS
account for access to {{site.course}} tools. 

### Request a CS account if you do not have one.

**If** you have **not** taken a Wellesley CS course before, please complete
the relevant form to request a Wellesley CS account:

- [CS account request for Wellesley students](https://cs.wellesley.edu/~sysadmin/casacct/account-request.php?cs240)
- [CS account request for cross-registered non-Wellesley students](https://cs.wellesley.edu/~sysadmin/accounts/account-request.html?cs240)

A [friendly human](https://cs.wellesley.edu/~anderson/) must respond
to your request, so please do this ASAP if it is needed.

### Connect to the CS GNU/Linux computing environment.

Since {{site.course}} studies low-level concepts, you need to use the
computing environment we have prepared to avoid incompatibilities
between the different low-level details in different computing
environments. While the concepts are general, concrete low-level
details differ among real systems. These real differences can cause
obvious or subtle problems if doing {{site.course}} assignment work in
a different environment.

You will see the following reminder of this fact in assignments:

{% include systems-env.md %}

You can complete the setup steps for this assignment and access the
{{site.course}} GNU/Linux computing environment either [locally in the
{{site.labspace.room}}](#local-access) or [remotely on your own
computer](#remote-access).

#### Local Access

During software-focused [lab]({{site.baseurl}}/lab/) exercises or
drop-in hours in the {{site.labspace.room}}, you will use the CS
GNU/Linux environment directly on the lab machines. These dedicated
machines have larger screens than your laptop. Instructors and tutors
will usually ask you to work on these machines for in-person code
help, since they allow partners or instructors to view your work
without invading your personal space to decipher a small screen in
your lap.

The lab machines in the {{site.labspace.room}} run GNU/Linux
(CentOS). Some can also run Windows.

- During the first few labs of the course, you will use a digital
  logic simulator that runs in Windows. Use your **Wellesley**
  username/password to log in.
- **For this assignment and all other parts of the course**, you will
  use software tools in GNU/Linux. Use your **CS** username/password
  to log in.

If you encounter a {{site.labspace.room}} machine that is running
Windows, reboot it. On startup, it will present a choice of which
system to launch. It will default to launching GNU/Linux (CentOS)
after a short wait.

#### Remote Access

If you wish to work outside the lab, you will connect remotely to the
same CS GNU/Linux environment.

- For beginners, we suggest following these [instructions to install
  and configure VSCode and its Remote Development extension on your
  computer for remote access to the CS GNU/Linux
  environment]({{site.baseurl}}/docs/common/vscode/). This allows
  you to edit files and run commands in the {{site.course}}
  environment (the CS server) from your computer while online.
- Alternatively, if you are already comfortable with the command-line
  and editing in the terminal with an editor such as
  [emacs]({{site.baseurl}}/docs/common/emacs/) or vim, you can skip
  VSCode and connect with [SSH]({{site.baseurl}}/docs/common/ssh/).
  (VSCode uses this under the hood.)

### Configure your CS account to use {{site.course}} software tools.

These tasks will get you started using the *command-line interface*
through a *terminal*. 

- If you are working locally, log into GNU/Linux (CentOS) on a machine
  in the {{site.labspace.room}} and open the Terminal application.
- If you are working remotely, [use VSCode with the Remote Development
  extension (or use SSH directly) to open a terminal connected to the
  CS GNU/Linux server.]({{site.baseurl}}/docs/common/vscode/)

At the text prompt in the terminal, type the following command exactly
and hit return/enter to run it:
    
~~~
source /home/{{site.id}}/live/env/init/account.sh
~~~
    
This is a one-time setup step that configures your CS account
permanently[^init-script] to find {{site.course}} tools. Regardless of
whether you connected locally or remotely, you do **not** need to
repeat it later.

### Learn some terminal basics.

{{site.course}} requires (but does not assume you have) some basic
terminal skills. This section gives a quick warm-up on the
command line with the CS GNU/Linux machines.
    
(*This part is adapted from past {{site.course}} lab activities by
Jean Herbst.*)

An operating system is the software which manages and controls all
other applications and resources on a computer. The computers we use
in {{site.course}} use the GNU/Linux operating system, which combines
the Linux kernel (the operating system core) with the GNU operating
system utilities.

For many {{site.course}} tasks, you will enter text commands directly
at a prompt in a terminal window, rather than selecting options with a
mouse through a graphical interface. A *terminal* is a session that
can receive and send input and output for command-line programs. A
*shell* is a program that is used for controlling and running programs
interactively via a terminal. Sometimes the two terms are used
interchangeably, though they are technically distinct.

Continue using the terminal you already have open.
    
For each of the following steps, try the specified command and
consider any questions listed. **You do not need to submit answers to
these questions anywhere.**.

Commands:

1.  `pwd`: **P**rint the **w**orking **d**irectory (the directory you
    are currently where you are currently working). The shell always
    *works* in one directory at a time. Your command prompt may also
    show this information.

1.  `ls`: List the files and subdirectories in your working directory.
     - How do you know whether the items listed are files or
       subdirectories?

3.  `ls -a`: Are there any files which begin with a `.`?

1.  `man ls`: Check the manual for all the different options for using
    the ls command. Type SPACE to advance one page, `q` to exit.

1.  `mkdir my-dir`: Make a new directory called `my-dir`

1.  `cd my-dir`: Change your working directory to the new directory.

1.  `echo YOURNAME`: (Replace `YOURNAME` with your name.) Echo your name to the screen.

1.  `echo YOURNAME > test1.txt`: Echo your name again, but this time
    redirect the output into a new file named `test1.txt`.

1.  List the contents of the directory to see whether the new file was
    created or not.  Then, use two different commands to display its
    contents to the screen (without an editor):

    - `ls`
    - `less test1.txt` (Page down with SPACE, exit with `q`.)
    - `cat test1.txt`
    
1.  [Check out additional
    reference](https://cs.wellesley.edu/~webdb/top/unix.html) (from CS
    304, ignore `drop`) or other [reference on the tools
    page]({{site.baseurl}}/tools/#general) as needed.

### Learn Git basics for {{site.course}}.

{{site.course}} uses the Git version control system to help manage
assignment code. We do not assume you have used Git before. Even if
you have used Git, there are some aspects of our workflow that will be
new to you.
    
Complete the [Git for Code
Assignments]({{site.baseurl}}/docs/common/git/#concepts) tutorial,
starting at
[*Concepts*]({{site.baseurl}}/docs/common/git/#concepts). Continue
through *First Steps* and *Solo Basics*, then ***stop just before
[Team Basics]({{site.baseurl}}/docs/common/git/#team-basics)***.


[^init-script]: If you prefer to use the {{site.course}} tools **only
    in the current shell session**, then run the command `source
    /home/{{site.id}}/live/env/init/session.sh` instead; run this
    command in each shell session where you want to use the
    {{site.course}} tools. If you need to remove the permanent account
    configuration, edit `~/.bash_profile` to remove the lines marked
    as related to {{site.course}}.

{:#course}
## Review Course Mechanics

{% comment %}
{% aside %}

The corner of each [{{site.course}}]({{site.baseurl}})
page contains buttons that jump to:

- the [&#x25b2;](#site-header) top of the page
- the [&#x2630;](#contents) table of contents, if any
- the [&#x25bc;](#site-footer) bottom of the page

*(Blue boxes mark optional asides.)*

{% endaside %}
{% endcomment %}

Read the [About page (syllabus)]({{site.baseurl}}) and other course info such as
the [course tools]({{site.baseurl}}/tools/) page, then answer the
following questions.

1.  What should you do if you can’t finish an assignment before the deadline?
1.  Is it ever acceptable to view another student’s code? If so, under what circumstances?
1.  Is it acceptable for two individuals to write a full code solution
    together on a whiteboard or shared document, then immediately type
    it up in their individual assignment submissions?
1.  When is it acceptable for one individual student to offer another
    individual student help understanding an error message?
1.  Give an example of when it is *not* acceptable to discuss
    high-level strategies for part of an assignment with another student.
1.  Why should you run {{site.course}} code on the CS GNU/Linux environment
    (remotely through VSCode/SSH from your computer or directly in
    the {{site.labspace.room}}) instead of installing a C compiler and
    working with code directly on your own computer?
1.  What is one good next step if compiling or running your code
    raises an error you do not understand?

You can answer these questions before completing account. To submit
answers, you must first complete the [account and tool setup](#setup).

**Submit your answers to this part in the file `course.txt` in your
`{{page.repo}}` repository.** To get your `{{page.repo}}` repository,
run this command in your CS terminal: `cs240 start {{page.repo}}
--solo`

{:#code}
## Make Nothing from Something

Before working on this task, you must first:

- Complete the [account and tool setup](#setup) above before this
  task; and
- Have some familiarity with basic concepts from the {{site |
  topic_link: 'bits'}} topic, which is [covered in
  class]({{site.baseurl}}) after this assignment is published.

In this part, you will do some brief C programming with [bitwise
operators]({{site.baseurl}}/topics/#bits) in the file `zero.c`. The
goal is to write several functions that take any byte value as an
argument, and, *no matter what that byte value is*, return the zero
byte, `0x00`. Of course this is trivial!  Just `return 0x00;`!

To make the task a little more interesting, we prohibit you from using
any operations except a small handful. As an example, the `zero_minus`
function must use only the MINUS operator, `-`, and the parameter,
`x`. (`return` and semicolon are also allowed and necessary.) No other
C features are permitted, not even literal number values like `0` or
`0x00`.

### Example

~~~ c
/**
 * EXAMPLE: zero_minus
 *
 * Goal: take an unknown byte value `x` and return the byte value 0x00
 * no matter what `x` is.
 *
 * The body of the function may use only `return`, `;`, `x`,
 * and the MINUS operator `-`.
 *
 * No other C features are allowed.
 */
byte zero_minus(byte x) {
  return x - x;
}
~~~

This function returns the zero byte (`0x00`) no matter what `x` it is
given.

For simplicity, we use the type `byte` (instead of the actual more
verbose C type `unsigned char`) to describe a 1-byte value in the
range `0x00` to `0xff`. Note: `byte` is *not* a standard C type
name. Comments in `zero.c` explain its definition.

### Task Specification

Your task is to replace the bodies of several more functions defined
in the `zero.c` file that should behave just like `zero_minus` but
with different restrictions:

- `zero_xor` uses only bitwise XOR (`^`).
- `zero_and_not` uses only bitwise AND (`&`) and bitwise NOT (`~`).
- `zero_or_not` uses only bitwise OR (`|`) and bitwise NOT (`~`).
- <sup>+</sup>Optional: `zero_plus_not` uses only integer addition (`+`) and bitwise NOT (`~`).

Any of these functions may use their parameter, `x`, as many times as
needed, and as many parentheses as needed.

**Hint: The simplest solutions are all quite short and easily fit on
one line.**

### Compiling and Testing

The `main` function in the file `zero_test.c` tests the behavior of
these zero-producing functions on all possible byte argument
values. An incorrect return value from any function causes the program
to print an error message.

Before we can run the testing code or any program based on our
`zero.c` code, we must *compile* the *source code* of the program to
produce a *binary executable*. The source code files contain
C-language code stored as human-readable text. An executable is a file
containing machine instructions encoded in binary. The compiler
translates from human-readable C code to machine readable executable
code. Once we have an executable, we can run it as many times as we
like.

#### Compiling an Executable from Source Code

To compile your puzzle solutions from `zero.c` with the test harness
in `zero_test.c` and produce a binary executable with testing code,
run the command `make`. This command uses compilation recipes in the
`Makefile` to produce an executable called `zero.bin`. If your code
compiles successfully, the session will look like this:

~~~ console
$ make
gcc -std=c99 -O0 -g zero.c zero_test.c -o zero.bin
~~~

If you have not changed `zero.c` since the last time you compiled, you will see:

~~~ console
$ make
make: 'zero.bin' is up to date.
~~~

Any other output indicates an error. Read the error message to determine how to proceed.


#### Running an Executable

Now that the executable `zero.bin` has been compiled, we can run it with
the command `./zero.bin`. Before you have made changes, expect to see:

~~~ console
$ ./zero.bin 
Testing zero_minus... all tests passed!
Testing zero_xor... at least one test failed.
  zero_xor(0x01) returned 0x01
  [Skipping remaining tests for zero_xor]
Testing zero_and_not... at least one test failed.
  zero_and_not(0x01) returned 0x01
  [Skipping remaining tests for zero_and_not]
Testing zero_or_not... at least one test failed.
  zero_or_not(0x01) returned 0x01
  [Skipping remaining tests for zero_or_not]
Testing zero_plus_not... at least one test failed.
  zero_plus_not(0x01) returned 0x01
  [Skipping remaining tests for zero_plus_not]
~~~

The provided puzzle functions after `zero_minus` do not work; you need
to change them so they do.

### Compilation is not Interpretation

Notice that this model of implementing a program by
*compilation* (translation to machine code) is different than the
model of *interpration* (running the source code directly), which may
be more familiar from languages like Python. Under compilation:

- The source code file and the executable file are separate files.
- The source code cannot be run directly. The hardware processor does
  not understand the source code. Only the executable can be run.
- The executable does not contain the source code and cannot be
  edited; it contains binary machine code that the hardware processor
  understands.
- The executable is produced or updated *only by explicitly compiling
  the source code*.
- If the programmer edits the source code file after compiling, the
  executable does not magically change. Running the executable runs
  the executable that was last compiled (before the change). The
  programmer must first compile the updated source code to create an
  updated executable if they wish to run the updated program.

**Every time you change `zero.c`** and want to run the tests again,
you **must run `make` to (re-)compile the executable before running
it.** If you do not, you will be running an old executable that was
compiled from an older version of your `zero.c` file. To help avoid
forgetting to compile, you may wish to run the `make test` recipe,
which compiles the executable if needed and runs it.

~~~ console
$ make test
gcc -std=c99 -O0 -g zero.c zero_test.c -o zero.bin
./zero.bin
Testing zero_minus... all tests passed!
Testing zero_xor... at least one test failed.
  zero_xor(0x01) returned 0x01
  [Skipping remaining tests for zero_xor]
Testing zero_and_not... at least one test failed.
  zero_and_not(0x01) returned 0x01
  [Skipping remaining tests for zero_and_not]
Testing zero_or_not... at least one test failed.
  zero_or_not(0x01) returned 0x01
  [Skipping remaining tests for zero_or_not]
Testing zero_plus_not... at least one test failed.
  zero_plus_not(0x01) returned 0x01
  [Skipping remaining tests for zero_plus_not]
make: *** [Makefile:43: test] Error 4
~~~

{% if visit %}
### Visit

{% endif %}


## Submission

You will use the standard code assignment submission process for your
answers about [course mechanics](#course) and [`zero.c`](#code).

{% include submit.md full=true %}

## Grading

This assignment is graded out of a total of 10 possible points for
completing all tasks. Parts with submitted answers may be graded for
correctness.

