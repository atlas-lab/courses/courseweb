---
toplevel: true
layout: page
title: Work
alt: "Assignment Work"
sym: "&#x1F3D7;"
# title: Code
# sym: "{}"
regenerate: true
---

{:.no_toc}
## Contents

- toc
{:toc}

{:.hidden}
## <a href="{{site.baseurl}}/topics/#material-by-topic">Lecture Preparation</a>

{:.hidden}
## <a href="{{site.baseurl}}/lab/">Lab Preparation</a>

## Assignments

{% aside %}
**Lecture preparation** is on the [Topics page]({{site.baseurl}}/topics/).

**Lab preparation** is on the [Lab page]({{site.baseurl}}/lab/).
{% endaside %}

{% assign code_tag = 'code' | type_tag %}
{% assign ex_tag = 'ex' | type_tag %}
{% assign lab_tag = 'lab' | type_tag %}
{% assign lec_tag = 'lec' | type_tag %}
{% assign exam_tag = 'exam' | type_tag %}

{% comment %}
{{ lec_tag}} denotes readings and class activities, listed separately on the [working schedule]({{site.baseurl}}/). \\
{{ lab_tag }} denotes pre-lab assignments and lab activities, listed separately on the [lab web page]({{ site.baseurl }}/lab/). \\
{{ ex_tag }} denotes written assignments, listed below. \\
{{ code_tag }} denotes applied assignments, listed below.
{% endcomment %}

<dl class="assignment_list">
{% assign all = site | assignment_list %}
{% for a in all %}
<dt><strong>{{ site | assignment_link: a }}</strong>: {{ a.desc }}</dt>
<dd>
{% for d in a.dates %}
<li>{{ d.first }}: <a href="{{ site.baseurl }}{{ site.schedule_url }}#{{ d.last | to_s }}">{{ d.last | date_string }}</a></li>
{% endfor %}
</dd>
{% endfor %}
</dl>

{:#retired-assignment-theme}
A note on assignments as of Fall 2020:

> Assignments in versions of this course from Spring 2015 through
  Spring 2020 integrated the theme of different courses at a school of
  magic from a popular book series, as we gradually showed that there
  is no magic inside a computer. The instructors decided it was time
  to remove this theme in Fall 2020. If you encounter any lingering
  traces that we missed, please let us know.


## Exams

<dl class="assignment_list">
{% assign all = site | exam_list %}
{% for a in all %}  {% comment %} Assumes exam IDs match course part/block IDs. {% endcomment %}
<dt><strong>{{ site | exam_link: a }}</strong>: {{ site.data.parts[a.id].title }}</dt>
<dd>
<ul class="date_list">
{% for d in a.dates %}
<li>{{ d.event }}: <a href="{{ site.baseurl }}{{ site.schedule_url }}#{{ d.date | to_s }}">{{d.time}} {{ d.date | date_string }}</a></li>
{% endfor %}
</ul>
</dd>
{% endfor %}
</dl>
