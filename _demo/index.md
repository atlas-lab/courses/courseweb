---
toplevel: true
layout: front
title: Calendar
alt: "Course Calendar"
sym: "&#x1F5D3;"
regenerate: true
front_links:
    - path: "/about/#acknowledgments"
      text: '👻 Boo! A button!'
      title: with title text, no less
messages:
    - waitlist.html
---

{% alert This is a demo of your future course website %}
Look into the beyond! Imagine the rickety infrastructure, creaking in the night! Good luck.
{% endalert %}

{% assign code_tag = 'code' | type_tag %}
{% assign ex_tag = 'ex' | type_tag %}
{% assign lab_tag = 'lab' | type_tag %}
{% assign topic_tag = 'topic' | type_tag %}
{% assign exam_tag = 'exam' | type_tag %}

<!--
<div class="cal_key">
    <a href="{{site.baseurl}}/about/#course-work">{{topic_tag}}</a>
    <a href="{{site.baseurl}}/about/#course-work">{{lab_tag}}</a>
    <a href="{{site.baseurl}}/about/#course-work">{{ex_tag}}</a>
    <a href="{{site.baseurl}}/about/#course-work">{{code_tag}}</a>
    <a href="{{site.baseurl}}/about/#course-work">{{exam_tag}}</a>
</div>
-->

{% include front-links.html %}
## Block Calendar
{{site | calendar_block: "part" }}

## Grid Calendar
{{site | calendar_grid }}

