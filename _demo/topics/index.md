---
toplevel: true
layout: tocpage
title: Topics
alt: "Lecture Topics and Materials"
sym: "&#x1F4DA;"
# sym: "&#x1F4D7;"
# sym: "&#x1F56E;"
regenerate: true
# group topics by "part" or by "block" ?
group_by: part
---

{% assign topic_tag = 'topic' | type_tag %}

{:.no_toc}
## Contents

- toc
{:toc}

## Material by Topic

This list of topics includes:

- Topics covered in class meetings, with preparation directions and
  listings of all lecture materials, associated readings, and
  activities.
- <sup>+</sup>Optional items that offer opportunities to explore further, but are not required.

{% comment %}
- Topics covered in lab that are **normally covered in lecture in
  full-semester versions of {{site.course}}**. We include all readings
  and lecture materials **for lectures in full-semester versions of
  {{site.course}}.** Labs do not necessarily use all of this material,
  but we include for reference.
{% endcomment %}

{::options html_to_native="true" parse_block_html="true" /}

{% assign block = null %}
<div>
{% assign topics = site | topic_labref_list %}
{% assign group_key = page.group_by %}
{% assign data_key = group_key | append: 's' %}
{% assign groups = site.data[data_key] %}
{% for x in topics %}
  {% assign id = x['topic'] %}
  {% assign t = site.data.topics[id] %}
  {% if t[group_key] %}
  {% if t[group_key] != block %}
    {% assign block = t[group_key] %}
    {% assign b = groups[block] %}
</div>
<h2 id="block-{{ block }}" class="{{ b['color'] }}_heading topic_block_heading"><a href="#block-{{block}}">{{ b['title'] }}</a></h2>
<div class="{{ b['color'] }}_section topic_block_section">
<div class="marge"></div>
  {% endif %}
  {% endif %}
{% assign lab_only_topic = x | lab_only %}
<h3 class="topic-heading" id="{{ id }}">{{ site | topic_link: id }} {% if lab_only_topic %}(lab-only){% endif %} {% if t.opt %}(optional){% endif %}</h3>
<div class="topic-dates">
{% for d in x.dates -%}
  <a class="block-link" href="{{ site.baseurl }}/#{{ d.date | week_start }}">{{ d.date | date_string }} ({% if d.context == "topic" %}Lecture{% else %}Lab{% endif %})</a>
{%- endfor %}
</div>
<div class="topic-content">
{% if lab_only_topic %}
*<strong>Lab-only:</strong> The {{t.title}} topic is covered only in lab. These materials are provided for **optional** reference in addition to the [lab materials]({{site.baseurl}}/lab/).*
{% endif %}
{% if t.opt %}
*<strong><sup>+</sup>Optional:</strong> This topic is an optional opportunity for further depth or exploration.*
{% endif %}

{% capture prep_path %}topics/_topics/{{id}}-prep.md{% endcapture %}
{% capture prep_relative_path %}_topics/{{id}}-prep.md{% endcapture %}
{% assign prep_path_exists = prep_path | exists %}
{:#{{id}}-prep}
#### Preparation
{% if prep_path_exists %}
{% include_relative {{prep_relative_path}} %}
{% else %}
{% if t.opt %}
No preparation.
{% else %}
*Coming soon...*
{% endif %}
{% endif %}

{:#{{id}}-materials}
#### Materials

{% if t.exercises %}
**Exercises**:
{% for e in t.exercises %}
- [{{e.title}}]({% if e.path %}{{site.baseurl}}{{e.path}}{% else %}{{e.url}}{% endif %})
{%- endfor %}
{% endif %}

{% capture slides4up_path %}slides/{{id}}-4up.pdf{% endcapture %}
{% assign slides4up_path_exists = slides4up_path | exists %}
{% capture slides_path %}slides/{{id}}.pdf{% endcapture %}
{% assign slides_path_exists = slides_path | exists %}

{% if slides_path_exist or slides4up_path_exists %}**Slides:** {% endif %}
{% if slides_path_exists %}<a class="topic_tag tag" href="{{site.baseurl}}/slides/{{ id }}.pdf" title="PDF of lecture slides for '{{t.title}}', 1 slide per page ({{id}}.pdf)">&#x278A; {{ id }}.pdf</a>{% endif %}
{% if slides4up_path_exists %}<a class="topic_tag tag" href="{{site.baseurl}}/slides/{{ id }}-4up.pdf" title="PDF of lecture slides for '{{t.title}}', 4 slides per page ({{id}}-4up.pdf)">&#x278D; {{ id }}-4up.pdf</a>{% endif %}

{% if t.playlist %}
{% include playlist.html title="Videos" playlist=t.playlist %}
{% endif %}

{% if t.page %}
**Readings:**

{% include_relative _topics/{{id}}.md %}
{% endif %}
</div>
{% endfor %}
</div>













{% comment %}
{:#missed-s20}
## What we missed in Spring 2020

Due to [mid-semester changes in response to the COVID-19
pandemic](https://www.wellesley.edu/coronavirus), we missed the following
topics and some [assignments]({{site.baseurl}}/assignments/#missed-s20)
usually included in {{site.course}}:

{% assign topic_ids = "buffer ecf process compiler-runtime" | split: " " %}
{% for id in topic_ids %}

{% assign t = site.data.topics[id] %}

{:#{{ id }}}
####  {{ topic_tag }} {{ t.title }}

##### Lecture

{% capture slides4up_path %}slides/{{id}}-4up.pdf{% endcapture %}
{% assign slides4up_path_exists = slides4up_path | exists %}
{% capture slides_path %}slides/{{id}}.pdf{% endcapture %}
{% assign slides_path_exists = slides_path | exists %}

{% if slides_path_exists or slides4up_path_exists %}**Slides:** {% endif %}
{% if slides_path_exists %}<a class="topic_tag tag" href="{{site.baseurl}}/slides/{{ id }}.pdf" title="PDF of lecture slides for '{{t.title}}', 1 slide per page ({{id}}.pdf)">&#x278A; {{ id }}.pdf</a>{% endif %}
{% if slides4up_path_exists %}<a class="topic_tag tag" href="{{site.baseurl}}/slides/{{ id }}-4up.pdf" title="PDF of lecture slides for '{{t.title}}', 4 slides per page ({{id}}-4up.pdf)">&#x278D; {{ id }}-4up.pdf</a>{% endif %}

{% if t.page %}
##### Reading:

{% include_relative _topics/{{id}}.md %}
{% endif %}
{% endfor %}
{% endcomment %}
