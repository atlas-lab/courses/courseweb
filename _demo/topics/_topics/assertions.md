- [How to use assertions in C](http://ptolemy.eecs.berkeley.edu/~johnr/tutorials/assertions.html), John Reekie.
- [The benefits of programming with assertions (a.k.a. assert statements)](http://pgbovine.net/programming-with-asserts.htm), Philip Guo.

For later:

- [C Programming Tips](http://pgbovine.net/c-programming-tips.htm), Philip Guo.
