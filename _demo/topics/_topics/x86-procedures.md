{% comment %}
**[Reading Quiz](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSfZc9dEYJVDuGhKVy15TEdJFp9ZfWBAx6Zl7kMItVyVoszBMQ/viewform)**

Please read/skim in advance of first class; complete **reading quiz by second class on procedures**.
{% endcomment %}

- Read: CSAPP 3.7
- Exercises:
  - Follow the examples in detail.
  - Consider this code:

          call next
        next:
          popq %rax

    Describe the (meaning of the) value that gets stored into `%rax`.
    Why is there no `ret` instruction matching the `call`?  Is this
    code useful? For what?
    This *not* how `call` is typically used, but it is a good test to
    check if you understand exactly what the `call` instruction does.
