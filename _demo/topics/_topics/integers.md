{% comment %}
[**Reading Quiz**](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSdZKF0d7iu52VkSm8WWtVFNU0XjY7KpwTJQMXWooauW937HmQ/viewform)
{% endcomment %}

{% comment %}
<!-- spring 2017, odd schedule -->

Remember [how to read]({{ site.baseurl }}/topics/#how).

Binary and hexadecimal number systems

- Read: [DDCA 1.4 - 1.4.5 (pages 9-15)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=32), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196),  or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA9)

Information as bits + context

- Read: CSAPP 2 - 2.1.2
- Exercises: CSAPP practice problems 2.1 - 2.4.  (Solutions at end of chapter.)
{% endcomment %}

{:#twos-complement}
#### For reference ***after*** the class on integer representation:

**Use this after class for reference.** We'd like to introduce signed
integer representations before you read about them.

As you read, focus on the *positional representation* of signed
integers more than the mechanics of how to convert from integer
representation to the representation you know well.

Read one of:

- "Twos Complement" section of [these notes](http://www.cim.mcgill.ca/~langer/273/1-notes.pdf).  Stop at "Floating Point."
- [Fundamental of Data Representation: Two's Complement](https://en.wikibooks.org/wiki/A-level_Computing/AQA/Problem_Solving,_Programming,_Data_Representation_and_Practical_Exercise/Fundamentals_of_Data_Representation/Two%27s_complement)
- SCO A.4 - A.5
- CSAPP's treatment of number representation is more thorough, but
  some students find it too dense and prefer to skip some parts.
    - Read: CSAPP 2.2 - 2.2.3.

Integer multiplication and division, relation to bitwise operations, sign extension

- Skim: CSAPP 2.2.4 - 2.2.8.
- Read: CSAPP 2.3.4-2.3.8

{% comment %}
<!-- #### [**Reading Quiz**](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSdwam8KXdkLE60cDYMxiuPX4abjyINUk_ySqFc53kfTELjLKQ/viewform) -->



Problematic in the past:

- Read: [DDCA 1.4.5 (pages 15-19)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=38), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196),  or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA15)
- Mechanical approach: [Two's Complement](http://www.cs.cornell.edu/~tomf/notes/cps104/twoscomp.html)
{% endcomment %}
