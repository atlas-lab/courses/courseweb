{% comment %}
#### [**Reading Quiz for First Class Day of Cache Material**](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSfVVulyNpc0ivAgPAA7KdLsDobSZI0hAt0kyln9ia5PYYNt_Q/viewform)
{% endcomment %}

#### **For First Class Day of Cache Material:**

- Skim: CSAPP 1.5-1.6, 6.1
- Read 6.2-6.4.3

#### **For Second Class Day of Cache Material:**

- Read 6.4-6.5
- Preview 1a and 1b in the upcoming lab assignment.  We'll see similar
  examples in class.
