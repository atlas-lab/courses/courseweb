{% comment %}
#### [**Reading Quiz**](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSdZKF0d7iu52VkSm8WWtVFNU0XjY7KpwTJQMXWooauW937HmQ/viewform)
{% endcomment %}

[How to read]({{ site.baseurl }}/topics/#how)

Binary and hexadecimal number systems

- Read: [DDCA 1.4 - 1.4.5 (pages 9-15)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=32), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196),  or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA9)

Information as bits + context

- Read: CSAPP 2 - 2.1.2
- Exercises: CSAPP practice problems 2.1 - 2.4.  (Solutions at end of chapter.)
- Skim: CSAPP 2.1.4 - 2.1.5.

Bitwise Boolean algebra and bit manipulation

- Read: CSAPP 2.1.6 - 2.1.9 (including the *Asides*.)
- Exercises: CSAPP practice problems 2.8, 2.9, 2.14, 2.16
- Optional: K&R 2.7, 2.9 for C reference
