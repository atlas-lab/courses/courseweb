{% comment %}
**[Reading Quiz](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSeZg8XhsnX-IDiaeioR8ORiCc1NTF6y_wBAcgQs2ioXVki-2w/viewform) for second class meeting on this topic**
{% endcomment %}

Background

- Skim: CSAPP 3 - 3.2

Data Movement

- Read: CSAPP 3.3 - 3.4 (including "New to C?" to help remember those pointers...)
- Exercises: try CSAPP practice problems 3.1, 3.2.  Take a look at practice problem 3.5, but don't spend too long on it.  We'll try more like this in class.

Arithmetic

- Read: CSAPP 3.5 - 3.5.4
- Exercises: try CSAPP practice problems 3.8, 3.9.  (Note `x <<= 4;` is the same as `x = x << 4;`.)
- Skim: CSAPP 3.5.5 
