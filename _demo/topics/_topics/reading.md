[home]: {{ site.baseurl }}

CS 240 moves quickly and covers a lot of ground.  To make class time effective, students should be ready to hit the ground running.  Class meetings and group exercises will assume cursory familiarity with basic ideas from assigned readings posted on the [course schedule][home].  Reading before class to learn basic mechanics frees up more time in class to apply or debug your understanding of these mechanics and to consider their deeper implications. 
{% comment %}
Regular reading quizzes will help motivate you to be prepared to participate.
{% endcomment %}

Bonus benefits of reading:

- Class is more fun if come with a rough idea of where we're going and what to watch out for along the way.
- Spending *x* minutes on reading now often saves multiples times *x* minutes of puzzlement or debugging later while working on assignments.
- We get to spend more time in class doing and less time listening to lectures.

Most topics on the [course schedule][home] have associated reading.  Effective reading for computer science courses demands a staged approach.  Aim for two types of reading:

- [Do an initial reading](#before-class) in preparation for the class when the topic is scheduled.
- [Revisit the reading](#after-class) later for deeper details or reference.

**Before class, do an initial reading** of the assigned reading material in preparation for class.  (No need to look over the slides before class.)

***Do not*** worry about understanding every last detail.

***Do*** aim to acquire:

- A big-picture view of the pieces we will consider about this topic.
- Some familiarity with basic mechanics of the ideas introduced.

To help distinguish core points from secondary concerns during initial reading, each reading is listed with one of two style directives:

{:#labels}
- ***Read*** means read for enough detail to do indicated [reading exercises](#reading-exercises).  **If you do get stuck or confused** by some details, do not worry.  Make a note and move on.  If we do not clear up your confusion in class, ask a question or come to office hours.
- ***Skim*** means read for high-level ideas.  Perhaps pick out a couple details that look interesting and accessible.  Do not spend much time trying to understand all the details before moving on.

Learning how to identify essential vs. inessential details during a first reading is an important skill that takes time to develop.  As the semester progresses, we will leave more of this to you.

Readings assignments may indicate specific **exercises to try** as you read before class.  These are typically practice problems from the reading.

- ***Try*** means work through enough of the exercise to see how the basics work.  Do not feel obligated to finish every example.  Do what is useful to you.  (Do not submit anything.)

We typically highlight exercises that practice mechanics.  Feel free to try other practice problems as well.  They may require more time and critical thinking.  We will explore such interesting examples in class.

{% comment %}
Short **reading quizzes** (administered electronically before class or on paper in class) count toward your grade.  They will be short and similar in style to reading exercises, focused on basic concepts and mechanics.  See the [syllabus]({{site.baseurl}}/syllabus/) for grading info.
{% endcomment %}

**After class**, revisit readings in more depth and try more practice problems to work out details as needed.


### More advice

Our main text (CSAPP) sometimes goes into more detail than we will cover, so learning to "read around" extra detail is a useful skill, especially in your pre-class reading.

When reading from **CSAPP**:

- "Asides" are optional. If you read them, *skim* them.
- "New to C?" blocks can be useful, but usually only if they are short.
- Some sections (*e.g.*, 2.2 - 2.4 on integer and floating point representations) can be too dense for our purposes.  We try steer you around them, but if you find other things getting dense, flip to another reading or just make a note and jump ahead.
- This book really shines with later material about machine/assembly language, caching, memory management, and other topics.  We use it intermittently in the first section of the course, then extensively for the latter two.


Tia Newhall (Swarthmore College) has more [good advice on reading computer science textbooks](http://www.cs.swarthmore.edu/~newhall/unixhelp/howto_readbook.php).



