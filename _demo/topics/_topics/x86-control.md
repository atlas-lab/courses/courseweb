Comparisons, Tests, and Jumps

- Read: CSAPP 3.6 - 3.6.4
- Exercises: Try CSAPP practice problems 3.13 - 3.14.  Just consider what operator you'd put in place of `COMP` or `TEST` to match the assembly code.  (Don't worry about `#define` etc. if you don't remember how macros work.)

Translating `if`s and loops

- Read: CSAPP 3.6.5, 3.6.7
- Exercises: CSAPP practice problems 3.16, 3.23, 3.24

Translating `switch` statements

- Skim: CSAPP 3.6.8, 3.6.6

