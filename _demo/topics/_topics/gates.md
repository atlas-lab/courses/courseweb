{% comment %}
[**Reading Quiz**](https://docs.google.com/a/wellesley.edu/forms/d/e/1FAIpQLSfTRXN7zNpks5PceVtQjWuPAdrAgCtYrDaEiEQWNEF9KAVqsQ/viewform)
{% endcomment %}

By our first full class day on this topic, you should be familiar with
at least the basics of logic gates, notation for Boolean expressions,
and a little Boolean algebra.

- Read: [Syllabus]({{site.baseurl}}/about/)
- Read: [How to Read for CS 240]({{site.baseurl}}/topics/#how)
- Read: [DDCA 1.5, 2.1-2.4 (pages 51-65)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=42), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196), or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA51)

#### Optional alternatives/extras for before or after class:

Try these if you want alternatives to the DDCA reading above or if you want some practice or you just can't wait until class.

- [SCO](#sco) 3.1.  Skip physical technologies: MOS, TTL, ECL, etc.
- Web pages covering roughly the same material with inline examples and exercises.
  1. Digital logic gates:
    - Bucknell EE: [An Introduction to Digital Logic - Signals and Gates](http://www.facstaff.bucknell.edu/mastascu/eLessonsHtml/Logic/Logic1.html). Stop at "Wiring a Quad NAND Gate."
    - Surrey EE: [Basic Gates and Functions](http://www.ee.surrey.ac.uk/Projects/CAL/digital-logic/gatesfunc/index.html)
    - [Digital Circuits and Boolean Logic](http://gk12.harvard.edu/modules/digitalcircuits.PDF).  Stop at "Implementing Gates."
  2. Boolean algebra
    - Bucknell EE: [Boolean Algebra](http://www.facstaff.bucknell.edu/mastascu/eLessonsHtml/Logic/Logic1.html#Boolean%20Algebra).
    - Surrey EE: [Boolean Algebra](http://www.ee.surrey.ac.uk/Projects/CAL/digital-logic/boolalgebra/index.html)
- Some more exercises
    - [Gates quiz](http://www.ee.surrey.ac.uk/Projects/CAL/digital-logic/gatesfunc/QuizFrameSet.htm)  (EXOR = XOR)
    - [Boolean algebra example, problem, and quiz](http://www.ee.surrey.ac.uk/Projects/CAL/digital-logic/boolalgebra/index.html#example)
- After class: Review sum-of-products (minterm) form and circuit simplicification as needed by reading [Logic Functions](http://www.facstaff.bucknell.edu/mastascu/eLessonsHtml/Logic/Logic2.html).

{% comment %}
#### Other Reference Material

- <http://american.cs.ucdavis.edu/academic/ecs154a.sum14/postscript/cosc205.pdf>
- <http://heather.cs.ucdavis.edu/~matloff/154A/PLN/DigLogic.pdf>

- [Digital Circuits/Logic Operations](https://en.wikibooks.org/wiki/Digital_Circuits/Logic_Operations)
- [Simple logic gates](https://en.wikibooks.org/wiki/IB/Group_4/Computer_Science/Computer_Organisation#Simple_logic_gates) (stop at "Combinational Circuits")
{% endcomment %}
