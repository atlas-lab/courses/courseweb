Read for general organization and design points about *instruction set architecture* and *microarchitecture*.  We will build our own toy architecture in class and lab.

- Read: [Central Processing Unit (Operation section)](https://en.wikipedia.org/wiki/Central_processing_unit#Operation)
- Read: SCO 2.1 - 2.1.2, 2.2.2, 5.1 - 5.1.2 (stop at "Note that having separate address spaces for instructions and data"), skim 5.1.3 - 5.1.4
- Alternatives:
  - Read: CSAPP 1.4.1, 2.1.0
  - [DDCA 7.3-7.3.3](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=126) ([alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196)) describes a similar microarchitecture for a similar 32-bit ISA, but relies on some detail from an extensive discussion of instruction set architecture in Chappter 6.  Our coverage will be more cursory.
