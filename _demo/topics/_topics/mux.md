Karnaugh Maps

- [DDCA 2.7 (pages 71-79)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=94), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196), or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA71) (missing pages 75-76)

Multiplexers and decoders

- Read: [DDCA 2.8 (pages 79-84)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=102), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196), or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA79) (missing pages 82-83)
- Read: SCO 3.2.2 (Combinational Circuits)
