- Read one of:
  - [DDCA 3.0 - 3.2.4 (pages 103-109)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=126), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196), or [on Google Books](https://books.google.com/books?id=5X7JV5-n0FIC&pg=PA103) (missing 103-104)
  - SCO 3.3 - 3.3.2 (3rd ed.) / 3.3.3 (4th ed.), Latches, Flip-Flops, Registers
- Read [DDCA 5.5 - 5.5.1 (pages 257-262)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=280), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196)  (ignore Verilog and VHDL).  We will cover RAM only briefly in class.
