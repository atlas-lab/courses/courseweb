General memory model:

- Read: SCO 2.2.2 - 2.2.3, 5.1.2 (stop at "Note that having separate address spaces for instructions and data")
- Read: CSAPP 2.1.0, 2.1.3 - 2.1.4


Mix and match to start learning about addresses and pointers in C:

- Read: CSAPP **3**.10.1
- Read [Pointer Basics](http://cslibrary.stanford.edu/106/) and watch the [silly video](https://www.youtube.com/watch?v=6pmWojisM_E) too.
- K&R 5 - 5.5
- [The Descent to C](http://www.chiark.greenend.org.uk/~sgtatham/cdescent/)
- [The 5-minute Guide to C Pointers](http://denniskubes.com/2012/08/16/the-5-minute-guide-to-c-pointers/)

{% comment %}
http://gribblelab.org/CBootCamp/8_Pointers.html
{% endcomment %}
