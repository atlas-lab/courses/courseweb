Adders

- Read one of:
  - [DDCA 5.1 - 5.2.1 (pages 233-234)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=256) up through Ripple Carry Adders (feel free to skim beyond if you are curious), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196) (ignore Verilog and VHDL)
  - SCO 3.2.3 (Arithmetic Circuits)
  - Digital Circuits: <a href="https://en.wikibooks.org/wiki/Digital_Circuits/Adders">Adders</a> up until Carry Lookahead Adder (feel free to skim beyond if you are curious)

Arithmetic Logic Unit

- Read one of these to understand the high-level organization of an ALU.  We will look at details of a specific ALU design in class.
  - [DDCA 5.2.2 - 5.2.4 (pages 23-234)](http://0-site.ebrary.com.luna.wellesley.edu/lib/wellesley/reader.action?docID=10251278&ppg=102) up through Ripple Carry Adders (feel free to skim beyond if you are curious), [alt. ebook](http://seeker.wellesley.edu:2060/patron/FullRecord.aspx?p=404196)
  - SCO 3.2.3 section on Arithmetic Logic Units
  - [Arithmetic Logic Unit](https://en.wikipedia.org/wiki/Arithmetic_logic_unit) sections "Signals" and "Circuit Operation"
