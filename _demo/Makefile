# FIXME: generalize

DEPLOYED = _remote
SOLUTIONS = $(DEPLOYED)/solutions

CONFIG_FILE = _config.yml
SSHID_CONFIG_KEY = sshid
INSTANCE_CONFIG_KEY = instance
SSHID = $(shell ruby -e "require 'yaml'; puts YAML.load_file('$(CONFIG_FILE)')['$(SSHID_CONFIG_KEY)']")
INSTANCE = $(shell ruby -e "require 'yaml'; puts YAML.load_file('$(CONFIG_FILE)')['$(INSTANCE_CONFIG_KEY)']")

RSYNC_REMOTE_URL = $(SSHID):archive/$(INSTANCE)/www
RSYNC_EXCLUDES = --exclude=/lab --exclude=/solutions \
					--exclude=.DS_Store --exclude='*\#*' --exclude='*~' \
					--exclude=/Makefile

# jekyll is dumb about symlinks in development mode.
# https://github.com/jekyll/jekyll/issues/6870
JEKYLL = env JEKYLL_ENV=production jekyll

PORT ?= 4000

local:
	$(JEKYLL) build --trace

serve:
	$(JEKYLL) serve --trace --port $(PORT) --livereload

# Temporarily symlink the parts of common.
# These links are not stored permanently in the repo in order to avoid confusing
# Jekyll's file watcher.
demo:
	mkdir -p common
	cd common && (for i in _includes _layouts _plugins _sass css icons; do test -e $$i || ln -s ../../$$i; done)
	$(JEKYLL) serve --trace --port $(PORT) --livereload
clean:
	rm -rf common

build-remote:
	rm -rf $(DEPLOYED)
	$(JEKYLL) build --destination $(DEPLOYED) --trace
	rm -rf $(SOLUTIONS)
	@test -z "$$(find $(DEPLOYED) -name '*solution*')" || (echo "FAILED: LINGERING SOLUTIONS" && find $(DEPLOYED) -name '*solution*' && echo "FAILED: LINGERING SOLUTIONS" && false)
	@echo "Automatically generated website.  Do not edit directly."  > $(DEPLOYED)/README.txt
	@echo "Website built for deployment."

rsync: build-remote
	rsync -avz --delete $(RSYNC_EXCLUDES) "$(DEPLOYED)/" "$(RSYNC_REMOTE_URL)/"
