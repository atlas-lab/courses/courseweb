---
toplevel: true
layout: page
title: About
alt: "About the Course: Syllabus, Policies"
img:
    path: common/icons/about.png
    alt: "&#x1F6C8;"
# sym: "&#x1F6C8;"
---

[www-health]: https://www.wellesley.edu/coronavirus/health#thethreews

{% assign late_day_cost_percent = 10 %}
{% assign code_tag = 'code' | type_tag %}
{% assign ex_tag = 'ex' | type_tag %}
{% assign lab_tag = 'lab' | type_tag %}
{% assign exam_tag = 'exam' | type_tag %}
{% assign topic_tag = 'topic' | type_tag %}

[cs240]: {{site.baseurl}}
[home]: {{site.baseurl}}
[lab]: {{"/lab/" | absolute_url}}
[class-deans]: https://www.wellesley.edu/advising/classdeans/staff
[lateform]: FIXME

{:.no_toc#contents}
## {{site.course}} {{site.semester}} <br />  {{site.course_title}}<small>[^oldnames]</small>

[^oldnames]: In the past, this course was named *Introduction to
    Commuter Systems*.

- toc
{:toc}

{:#about}
## What is {{site.course}}, {{site.course_title}}?

{{site.description}}

### Goals

Course goals include:

* Communing with computational spirits.
* To understand the roles of key software and hardware abstractions
  in otherworldly communication.

### Topics

Computer Seances, or whatever else you want to see.

### When Should You Take {{site.course}}?

**Prerequisite:** Faith in the beyond.

**Recommendations:** Run!

Please talk to the [instructors](#staff) if you have any questions or
concerns{% comment %} or if you are considering credit/non grading {% endcomment %}. We are happy
to work with you to make your time in {{site.course}} most ethereal
and rewinding for you.

{:#staff}
## Course Staff

{% include staff-online.md %}

The {{site.course}} instructors and tutors look forward to welcoming
you to {{site.course}} and joining you as you explore how computers
work. We hope to see you regularly in and out of scheduled course
meetings.

Please follow announcements, get in touch, find support in drop-in
hours, or make a private appointment with the instructors or tutors in
the {{site.course}} venues for [communication and
support](#communication).

## Course Meetings

{:#class-meetings}
### Class Meetings

If classroom capacity allows, you may attend either lecture section,
regardless of which is your officially registered section.

{% for c in site.meetings.classes %}
{%- assign instructor = site.instructors[c.instructor] -%}
- Lecture section {{forloop.index}}: {{ c.days }} {{ c.time }}, {{c.where}}, [{{instructor.first}} {{instructor.last}}](#staff)
{% endfor %}

The [course calendar]({{site.baseurl}}) lists topics for each class
meeting, linked to supporting materials and assigned preparation.

**Please center your aura before entering the classroom.**

{% if site.meetings.labs %}
{:#lab-meetings}
### Lab Meetings

You must attend the lab section for which you are registered.

{% for c in site.meetings.labs %}
- Lab section {{forloop.index}}: {{ c.days }} {{ c.time }}, {{site.labspace.room}}, [{{site.labinstructor.first}} {{site.labinstructor.last}}](#staff)
{%- endfor %}

[Lab logistics, policies, and materials are on the lab
webpage.]({{site.baseurl}}/lab) Lab attendance is required.

{% endif %}

{:#communication.clear}
## Communication and Support

There are a few venues for communicating with your classmates, tutors,
and instructors in {{site.course}}. To find support, check these
resources:

1.  Start with the course website for the [course
    schedule]({{site.baseurl}}/) and documentation on [assignment
    work]({{site.baseurl}}/assignments/),
    [labs]({{site.baseurl}}/lab/), [course
    policies]({{site.baseurl}}/about/), [software
    tools]({{site.baseurl}}/tools/), and [course
    topics]({{site.baseurl}}/topics/).

1.  Course communcation outside class happens on the
    **[{{site.course}} {{site.forum.name}}]({{site.forum.url}})**.
    Ask and answer questions, read announcements, discuss course
    topics, and send private messages on {{site.forum.name}}. **Plan
    to follow announcements regularly.** See the [{{site.forum.name}}
    guidelines](#forum).

{% comment %}
1.  The **[{{site.course}}
    {{site.labspace.online.name}}]({{site.labspace.online.url}})** is
    a [Zoom video meeting used for all remote events in
    {{site.course}}](#cafe). Participate in class meetings, seek
    academic support in [drop-in hours with instructors and
    tutors](#drop-ins), or just hang out in the
    {{site.labspace.online.name}}. See the
    [{{site.labspace.online.name}} guidelines](#cafe).
{% endcomment %}

1.  Tutors and instructors are available for [drop-in
    hours](#drop-ins) in the {{site.labspace.room}}.

1.  Instructors are also available for private appointments, arranged
    via private communication (Zulip private message or email).
    
{% comment %}
You will learn in the first couple days of {{site.course}} how to make
sense of the silly names.


Instructors will make announcements during lab and class meetings or
on [{{site.forum.name}}](#forum) or course email list.  Students
who are registered (or anticipated to register from the waitlist) will
be added to these communication groups before the first course
announcements and meetings.
{% endcomment %}

{% comment %}

**Find materials** on this website, organized chronologically on the
[course calendar]({{site.baseurl}}/) and by category via links at the
top and bottom of each page. Some materials for lab activities will be
sent directly via the email list instead.

{% endcomment %}

{:#forum}
### [{{site.forum.name}}]({{site.forum.url}})

Course communication outside class happens on the **[{{site.course}}
{{site.forum.name}}]({{site.forum.url}})** in these
[streams](#using-zuip):

{% comment %}  
> What's with the silly name? 240<sub>10</sub> = [F0<sub>16</sub>,
written `0xF0` in programmer's hexadecimal
notation]({{site.baseurl}}/topics/#bits). Thus "240 Forum" = "<span
class="tt">0xF0</span> Forum" = "<span class="tt">0xF0rum</span>" =
"<span class="tt">0xF0</span> room" = "240 room". &#x1F644;

The {{site.forum.name}} is organized into [streams](#using-zulip):
{% endcomment %}

1. **`#announcements`**: course announcements from instructors or tutors (Follow this stream regularly!)
1. **`#questions`**: questions and responses/discussion about
  logistics, concepts, course work (do not post your code or
  complete/partial solutions), error messages, or software tools
1. **`#partner search`**: find partners for pair work
1. **`#community`**: everything else, hanging out, levity
  
Collaborating to ask and answer questions on {{site.forum.name}}
helps students, tutors, and instructors help each other by pooling our
collective curiosity, experience, knowledge, and critical
thought. Please engage enthusiastically, subject to
these guidelines:

1.  Communicate respectfully and patiently with the goal of supporting others and yourself.
    - Asking a question is just as valuable a contribution as answering one.
1.  Before posting:
    - Make an effort to check appropriate documentation on [assignment
    work]({{site.baseurl}}/assignments/),
    [labs]({{site.baseurl}}/lab/), [course
    policies]({{site.baseurl}}/about/), [software
    tools]({{site.baseurl}}/tools/), or [course
    topics]({{site.baseurl}}/topics/).
    - Check to see if others have already asked or answered your question.
1.  Do not post complete or partial solutions to assignment work
    in the form of code, short answers, or detailed descriptions of
    algorithms in any human or programming language. Posting sections
    of starter code or code from course materials with attribution is
    OK.
1.  Use private messages, email, or appointments with instructors to
    communication about private topics such as grades, extensions, or
    personal issues.

#### Using Zulip
  
[Zulip messages belong to a *topic* within a
*stream*.](https://zulip.com/help/getting-started-with-zulip) It's
like a more organized Slack also capable of long-form messages or a
discussion forum also capable of quick chat. The 3-level organization
allows flexible focus by selecting:
   
- A topic: view only the messages within that topic chronologically.
- A stream: view all messages from all topics in that stream chronologically.
- *All Messages:* view everything chronologically.
     
Hygiene:
   
- Reply (shortcut: `r`) to keep your message in the same topic; use a new topic for an unrelated message.
- Move a message to a different topic or stream after the fact if it is out of place.
- Formatting posts with [Markdown or
  LaTeX](https://zulip.com/help/format-your-message-using-markdown)
  can help readability, such as to separate `code(snippets)` from
  prose.
- If showing a code example (not solution code), an error message, or
  other text, please post it as text (**not** a screenshot). Text is
  searchable, zoomable, reflowable, and more. Screenshots are much
  less useful than text. Preserve indentation, etc., by using [simple
  code
  formatting](https://zulip.com/help/format-your-message-using-markdown#code)
  in your message.  [Here's an example.](text-not-screenshots.html)

{% comment %}
{:#cafe}
### The [{{site.labspace.online.name}}]({{site.labspace.online.url}}) (Zoom Meeting)

The **[{{site.course}}
{{site.labspace.online.name}}]({{site.labspace.online.url}})** is a
[Zoom video meeting](https://www.wellesley.edu/lts/techsupport/zoom)
used for all remote {{site.course}} events. Participate in [class
meetings](#class-meetings), seek academic support in [drop-in hours
with instructors and tutors](#drop-ins), or just hang out in the
{{site.labspace.online.name}}.

> *Join us for a byte in the {{site.labspace.online.name}}, where we
can chat face to `0xFACE` [`&`
un**mask**ed]({{site.baseurl}}/topics/#bits) over live video `0xFEED`!*
(But do a wear a mask if you're in a public space!)
>
> What's with the silly name? "Cafe" is the new term for all
[PLTC](https://www.wellesley.edu/pltc) drop-in tutoring
resources. Spelling words in programmer's hexadecimal notation is
fun. We will learn about [hexadecimal, the `&` operator, and *bit
masking*]({{site.baseurl}}/topics/#bits) in the first couple days of
{{site.course}}. &#x1F644;
    
Guidelines for all meetings in the {{site.labspace.online.name}}:

1.  [Class meetings](#course-meetings) and [drop-in hours](#drop-ins)
    use the {{site.labspace.online.name}}.  {{site.course}} students
    may use the {{site.labspace.online.name}} outside of scheduled
    course activities to gather, ask each other questions, work with
    others, or hang out.
1.  Do not share meeting links or passwords outside the class.
1.  If you have a set of external headphones/microphone that's better
    than your built-in computer audio (especially microphone), use
    them.
1.  Try to join from an environment that is relatively quiet and
    non-distracting. Sometimes this is hard; do your best with what's
    available.
1.  Do not join a meeting from the same physical space as another
    person who has joined the same meeting unless you both use
    headphones (to avoid audio feedback problems).

{% endcomment %}

{:#drop-ins}
### Drop-in Hours

[Regularly scheduled **drop-in hours** with tutors or
instructors]({{site.baseurl}}/support/) are times for you to visit for
any kind of questions or academic support (large or small) on course
work, concepts, and beyond. [You can reserve some times in advance or
just show up during scheduled hours.]({{site.baseurl}}/support/) See
the [Help]({{site.baseurl}}/support/) page.

{:#support-style}
### How We Support You

The instructors and tutors play a few roles in {{site.course}}:

- We welcome you to the world of computer systems and serve as
  your guides to new ways of thinking, new ideas, and new skills as
  you explore this world.
- We pose challenges to help you develop new thought processes,
  knowledge, skills, and independence as computer scientists.
- We provide support for you to learn, grow, and meet these challenges
  under your own power, because we believe every one of you can do so.
  
**Seeking academic support from peers, tutors, or instructors is a
normal part of the {{site.course}} experience.** Here is what to expect:

**We ask you questions.** The {{site.course}} staff enjoy helping you
[learn independently](#compact), so we often answer your questions by
asking you more questions to help you construct your own path
forward. While this may feel less expedient in the short term, it is
more effective in the long term, since you learn a broader thought
process rather than one small answer.

{:#prep-expectations}
**Preparation is your ticket to assistance and curiosity pays off.** 
In general, we expect you to make a [genuine effort](#compact) to
answer your own question before asking for help on assignment work.  It
is rarely wise to spend hours stumped on a single issue before asking
for help, but at least a few minutes of common sense exploration often
nets effective results or at least helps focus your question. Be curious!
Compiler error message?  Try looking it up in the assignment,
associated tool documentation, or a web search to understand what the
error means.  Crashing program?  Try to employ debugging strategies and
tools listed in the assignment to make progress toward understanding
the cause.

Each code assignment includes a required preparatory exercise to help
understand foundations for the assignment.  Lab or class activities
often spend time on these exercises.  To make sure you understand the
preliminaries, instructors and tutors require that you complete this
preparation before asking for support with the main assignment tasks.
We are happy to answer questions about the preparation itself at any
time, but we will defer questions on core assignment tasks until your
preparation is complete.

{:#levels}
**Levels of support.** Tutors and instructors provide different levels
of support for different parts of assignments. Preparatory exercises
enjoy broad support. The staff will answer questions and provide
assistance on the main part of assignments in accordance with
principles in the [honor code definitions](#honor-code). Tutors and
instructors will answer only clarifying questions on **[Independent]**
parts of assignments.


## Course Materials

All [in-class material]({{site.baseurl}}/topics/) and
[assignment work]({{site.baseurl}}/assignments/) is linked from the
[calendar][home] on the [course website][home].  [Lab material][lab]
is shared ahead of each lab meeting.

### Texts

Just imagine them.

### Acknowledgments

This course website uses [<s>course web infrastructure</s> a
several-year sedimentary accretion of time-constrained
hacks](https://gitlab.com/atlas-lab/courses/courseweb) developed by
[Ben Wood](https://cs.wellesley.edu/~bpw/).

## Course Work

{% comment %}
{{topic_tag}} {{lab_tag}} {{ex_tag}} {{code_tag}} {{exam_tag}}
{% endcomment %}

**Basic practice:** Class and lab work help you learn the basic concepts.

- [**Class preparation and activities**, described with the logistics
  of class meetings](#class-meetings), include watching videos,
  reading, trying small exercises, and participating in larger pair
  exercises during classes.

- [**Lab preparation and exercises**, described on the lab
  webpage]({{site.baseurl}}/lab/), include preparatory lab assignments
  to complete ahead of each lab meeting and lab reports written during
  lab activities.

**Synthesis:** Larger assignments push you to connect
concepts and apply them in practice.

- **Code and written assignment work** applies computer systems concepts in hardware and
  software, forming the heart of {{site.course}}.
  - Each code assignment has a preparatory exercise to ensure you
    understand the preliminaries before beginning the main
    assignment. [Completing the preparatory exercise is required to
    receive support on the main assignment.](#prep-expectations)
  - Class and lab activities often integrate with assignment work,
    especially preparatory exercises.
  - Parts marked as **[Independent]** must be completed by you on your
    own, without support from instructors, tutors, or anyone other
    than your partner, if working as a pair.

**Examinations:** Two asynchronous exams are listed on the [course
calendar][home]. The exams are available over several days to remove time
pressure and provide flexibility.

**The [Honor Code applies to {{site.course}} course work as defined
below](#honor-code).**

A note on assignment work as of Fall 2020:

> Assignments in versions of this course from Spring 2015 through
  Spring 2020 integrated the theme of different courses at a school of
  magic from a popular book series, as we gradually showed that there
  is no magic inside a computer. The instructors decided it was time
  to remove this theme in Fall 2020. If you encounter any lingering
  traces that we missed, please let us know.

{% comment %}
- Written "paper" assignments explore concepts in the small.
- Applied code assignments run 1-2 weeks and involve
  [ **significant effort**](#compact) on programming, reverse
  engineering, and debugging.
  
**Exams (35%)**: Exams, marked on the [calendar][cs240], cover:

1. {{exam_tag}} 
   [computer hardware implementation]({{site.baseurl}}/topics/#part-hw)
2. <span class="deletion">{{exam_tag}}
   the hardware-software interface plus
   memory hierarchy and cache</span> <span class="addition">(canceled)</span>
{% endcomment %}

{:#grades}
### Grades

{::options parse_block_html="true" /}
<div style="padding: 1em; float: right; background: white;">

| **Percentage** | **Letter** |
| &ge; 95.0 | = A |
| &ge; 90.0 | &ge; A- |
| > 86.6 | &ge; B+ |
| > 83.3 | &ge; B |
| &ge; 80.0 | &ge; B- |
| > 76.6 | &ge; C+ |
| > 73.3 | &ge; C |
| &ge; 70.0 | &ge; C- |
| &ge; 60.0 | &ge; D |
| < 60.0 | &ge; F |

</div>
{::options parse_block_html="false" /}

{% comment %} **Course grade derivation:** Within each category of
[course work](#course-work), individual units are weighted according
to their labeled point value.  {% endcomment %} 

Course grades are computed by weighting course components as follows:

- Lab preparation and activities: 15%
- Assignment work: 45%
  - Assignments are weighted by listed point values.
- Exams: 40%

Course grade percentages are converted to letters as shown in the
accompanying table. The minimum percentage required to achieve a given
letter grade may be adjusted downward (only to raise letter grades),
but never upward (never to lower letter grades).  If your percentage
grade satisfies the constraint in the left column, then your letter
grade will satisfy the constraint in the right column of the same row.

{% comment %}
Each student's grade is determined independently without consideration
of other students' grades.


Note that in the 2020-2021 academic year, the deadline to declare
credit/no-credit grading falls on the last day of classes.

[Recall](http://www.wellesley.edu/registrar/grading/grading_policy#system)
that, the minimum grade to earn credit under *credit/no credit*
grading is C.  The minimum grade to pass and earn credit under normal
letter grading is D.

#### Anonymous Grading
{% endcomment %}

**Anonymous Grading:** {{site.course}} staff make a best effort to
grade course work anonymously, meaning that, under normal
circumstances, graders do not associate student identity with
submitted work until all on-time submissions for that assignment/exam
have been graded in full. Once anonymous grading is complete, each
student is identified and the unadjusted results are attached to the
student's record.

Anonymous grading helps improve consistency and fairness, reduce
effects of any implicit bias, and make clear that the course staff is
grading your *work*, not *you*. To assist in maintaining anonymity, it
is important to avoid listing your name within your work except where
specified, so we can automatically hide it during grading.

**Limitations of anonymity:** Anonymous grading in {{site.course}} is
"best effort" in that the graders can look up the student identity
associated with any submission at any time, but we avoid it under
normal circumstances. We try to detect submission problems early,
before grading begins, so that they can be resolved without affecting
anonymity. Nonetheless, anonymity can be compromised in cases where we
discover a submission problem only later during grading and request
that you resubmit, or when you submit late work after we graded
on-time submissions.

{:.clear#late}
### Late Policy

Assignment work is due at the date and time specified in the
assignment manifest. Deadlines synchronize with class material and
scheduled academic support (drop-in hours) to help ensure smooth
forward progress in the course. You are **strongly advised** to keep
up with deadlines. {{site.course}} moves quickly. Falling behind on
assignment work can make the work more difficult.


Sometimes, however, prioritizing completion of an assignment by the
deadline may not be the right personal choice for you. To encourage
you to learn from the assignments while also affording flexibility
when it benefits your health and well-being, you may use either of the
following mechanisms to submit late **(non-lab)** assignment work without penalty:

1. **Self-serve 48-hour late pass**: You may delay any assignment
   deadline by 48 hours by submitting this **[late pass notification
   form][lateform]** **before the original deadline.** There is no
   need for any other communication in this case.
   - The form requires a **brief** summary of progress (e.g., "#1
     done, #2 stuck, #3 started" or "not started") and whether you
     anticipate seeking support (drop-in hours/appointment) for the
     assignment (yes/no).
   - If the 48-hour extended deadline falls during a break or weekend,
     it is automatically moved to the final day of that break or
     weekend.
2. **Custom extension:** If you need to extend an assignment
   deadline more than 48 hours, or if you did not submit a late pass
   notification in time, you must email the instructor with an initial
   timeline and plan for when and how you will complete the
   assignment and report on progress.
   - We will adjust the plan together to make sure it is
     reasonable. The instructor may ask you to prioritize current work
     first. If you need custom extensions on more than assignments,
     the instructor may ask you to check in with your [class
     dean][class-deans] to make sure you are getting the support you
     need to manage your course load.
   - You are never required to discuss details of your personal
     circumstances with an instructor to receive an extension, although we are
     happy to lend an ear. If you choose to share, please note that
     [reporting duties](#reporting) prevent instructors from holding strict
     confidentiality in all cases.
   
The above extension mechanisms are subject to the following limitations:

1. No extension may continue later than 48 hours beyond the official
   last day of classes unless your [class dean][class-deans] supports
   it.
2. Feedback and grading for work submitted under an extension will be
   completed eventually. This may take arbitrarily long, possibly
   past the end of classes and exams. The likelihood and likely
   magnitude of delay grow with the length of the extension.

**Preparatory assignments** for [lab][lab] or class meetings are
**not** subject to these policies, since their timeliness 
matters for making best use of the associated lab/class meeting.

**Exams** are **not** subject to extensions. Exam dates are fixed, as
noted on the [course calendar][home]. Mark them on your calendar.
Please arrange accommodations with the instructor at least 3 days
ahead of exams. If an emergency prevents you from completing the exam
as scheduled, we can schedule a make-up exam with the support of your
[class dean][class-deans] or [health
services](https://www.wellesley.edu/healthservice). This sets a common
standard for all students and ensures you are getting the support you
need for your emergency.

{:#honor-code}
### Honor Code and Collaboration/Resource Policies

**The [Wellesley Honor
Code](https://www.wellesley.edu/studentlife/aboutus/honor) applies to
{{site.course}}, including
through {{site.course}}-specific policies for acceptable resources
(collaboration, assistance, and reference) in course work**. The
general spirit of these policies should not be surprising, but the
details are key to boosting the quality of your learning and
maintaining a fair and rigorous academic environment for you and your
classmates.

In summary:

- **[Ungraded practice activities](#ungraded-practice)** support
  initial learning, so they are **free of restrictions**.
- **[Graded assignments](#graded-assignments)** support deeper
  learning and independence, so they **require that your submitted
  work be your own**, following one of two policies for acceptable
  resources:
  - **[Graded practice assignments](#graded-practice)** help you
    cement the basics and learn relevant tools, so they **permit
    relatively broad resources**.
  - **[Graded synthesis assignments](#graded-synthesis)** require you
    to think critically and make connections to formulate logic and
    implement solutions independently, so they **restrict acceptable
    resources significantly**.
    - **[Independent] parts** of graded synthesis assignments must
      additionally be completed **without assistance from others**.
- **[Graded examinations](#graded-examinations)** measure your independent
  mastery of concepts, so they **require that your submitted work be
  your own, without using any resources**.
  
Full definitions are linked from the summary and appear below.  Please
read the entire policy. If in doubt about details, please ask for
clarification.  Violations of these policies are handled through the
appropriate [Honor Code charge or resolution
processes](https://www.wellesley.edu/studentlife/aboutus/honor/procedures).


{:#ungraded-practice}
#### Ungraded Practice Activities

**Types:** reading, study, class exercises
  
**Policy:** You may collaborate, receive and provide assistance, and
  consult reference without restriction.
  
{:#graded-assignments}
#### Graded Assignments

**Types:** [graded **practice** assignments](#graded-practice), [graded
**synthesis** assignments](#graded-synthesis)

**Policy:**

- A ***team*** works as one to complete and submit one assignment
      solution. [A team is an ***individual*** or a ***pair***,
      determined by the assignment's designated team
      policy](#team-policy).
- Your team's submitted solutions must be **your team's own work**.
- You **must not divide work** among people (within or across teams)
  to complete separate parts and combine the solutions.
- Your submissions must **cite all collaboration, assistance, and
  reference** (excluding course materials and reference linked from
  the course website) that your team used to prepare your submission.
  List each person or resource with a short phrase identifying the
  topic or nature of collaboration, assistance, or reference.

##### Team Policy

Each graded assignment follows one of two **team** policies. Graded
synthesis assignments indicate their policy in the assignment
manifest.

- The **individual** policy requires individual students to prepare
  and submit their own solutions.
- The **pair** policy allows two students to prepare and submit a
  single joint solution.
  - Individuals may opt to complete an individual submission on pair
    assignments unless pairs are explicitly required.
  - Partners choosing a pair submission must contribute in similar
    measure and complete at least 80% of work on each problem together
    in real-time pair-programming style. Collaboration within a pair
    is otherwise unrestricted.
  - If it becomes necessary to split a pair, the partners must
    coordinate with the instructor to document what work was completed
    together.
  
{:#graded-practice}
##### Graded Practice Assignments

{:#lab-policy}
**Types:** [individual](#team-policy) pre-lab assignments,
[pair](#team-policy) lab activity reports
      
**Policy:**

- All parts of the [general graded assignment
  policy](#graded-assignments) apply.
- You may collaborate with other {{site.course}} {{site.semester}}
  students, including to develop solutions together, if collaborators
  participate in roughly similar measure.
- You may provide and use assistance or reference in accordance with
  the above policies without further restriction unless otherwise
  noted on the assignment.

##### Graded Synthesis Assignments

**Types:** written assignments, applied code assignments

**Policy:**

- All parts of the [general graded assignment
  policy](#graded-assignments) apply.
- Collaboration:
  - On **all** parts:
    - You must not communicate detailed algorithms, implementations, code,
      formulae, or other detailed solution steps with other teams.
    - You must not, under any circumstances, view, share, prepare, or
      accept written solutions or assignment code outside your team.
  - On parts **not** marked [Independent]:
    - You may discuss high-level ideas or strategies with other teams in
      small groups.
  - On parts **marked [Independent]**:
    - You may not give or receive assistance on the [Independent] part.
- Assistance:
  - On parts **not** marked [Independent]:
    - You may accept help from course staff to locate errors, interpret
      error messages, and correct errors of syntax.
    - You may accept help from other {{site.course}}
      {{site.semester}} students to interpret error messages, subject
      to the above policy on viewing code.
  - On parts **marked [Independent]**:
    - You may accept help from course staff to only to interpret
      error messages or receive answers to clarifying questions.
- Reference, on **all** parts:
  - You may consult course material from {{site.course}}
    {{site.semester}} and external documentation of required tools.
  - You may consult external reference resources *for general concepts
    and techniques*, provided you cite them.
  - You must not consult solutions to this or any similar assignment.
  - Code reuse and adaptation:
    - You may reuse and adapt provided starter code in your solution
      without restriction.
    - You may reuse and adapt code from other course materials only
      *with prior written approval* from the instructor and *explicit
      attribution* of the source.
    - You must not reuse or adapt any other code.

**For non-[Independent] problems only**,
you are welcome to discuss *high-level* ideas and strategies with
other teams, as allowed by the policy above. If taking notes on these
discussions, keep your notes general (no verbatim copies, photographs,
detailed descriptions).  Remember, you may not write solutions
together, so later, write full solutions on your own, referring to
your notes only if needed.

**Rules of thumb for collaborating outside your team** on non-[Independent] parts of graded synthesis assignments:

  - Wait at least 30 minutes after discussions outside your team
    before writing your solutions. This helps you know whether you
    actually understand the solutions (and their derivation) yourself.
  - If writing your solution is effectively making a verbatim copy of
    -- or following a set of explicit directions in -- your notes from
    discussion with others, then your notes or your discussion are too
    detailed.

**After the course:**

Some students students may wish to include work from {{site.course}}
in portfolios, resumes, etc.  Please do not post code publicly
(including on GitHub, GitLab, Bitbucket, your own website, etc.), but
feel free to show your code privately to potential employers or others
who are not students.  Do not make your code accessible or viewable by
current or future {{site.course}} students.

{:#graded-examinations}
#### Graded Examinations

{% comment %}
**Types:** in-class exams, take-home exams
{% endcomment %}
**Types:** asynchronous exams

**Policy:**

- Your submitted solutions must be your own work.
- You must not provide or use any collaboration, assistance, or
  reference external to the exam document between receiving and
  submitting the exam unless explicitly stated by the exam document
  itself.
- You must not communicate about the exam contents with anyone other
  than the {{site.course}} {{site.semester}} instructors until given
  permission by a {{site.course}} {{site.semester}} instructor.
      
## Accommodations

### Accessibility and Disability

If you have a disability or condition, either long-term or temporary,
and need reasonable academic adjustments in this course, please
contact Disability Services to get a letter outlining your
accommodation needs, and submit that letter to me. You should request
accommodations as early as possible during the course, or before the
course begins, since some situations can require significant time
for review and accommodation design. If you need immediate
accommodations, please arrange an appointment with me as soon as possible. If
you are unsure but suspect you may have an undocumented need for
accommodations, you are encouraged to contact Disability Services.
They can provide assistance including screening and referral for
assessments.  Disability Services can be reached at
disabilityservices@wellesley.edu, at 781-283-2434, by scheduling an
appointment online at their website <https://www.wellesley.edu/disability>.
{% comment %}
, or
by visiting their offices on the 3rd floor of Clapp Library, rooms 316
and 315.
{% endcomment %}

### Religious Observance

Students whose religious observances conflict with scheduled course
events should contact the instructors in advance to discuss
alternative arrangements.  You may do this through the [Wellesley
College Religious Observance Notification
system](https://webapps.wellesley.edu/religious_calendar/) if you
prefer.


### Pandemic Circumstances

The course staff recognizes that current-stage pandemic circumstances
may continue to be, overall, more stressful than the baseline in other
years and that the impacts may vary substantially among students.
  
Please be in touch with us if you are concerned about the impact of
your circumstances on your ability to participate in
{{site.course}}. We will work with you to determine a reasonable
approach for you and {{site.course}}.

{:#compact}
## How to Succeed in {{site.course}}
{% comment %}
## The {{site.course}} Compact
{% endcomment %}

**Challenging Assignments:** We study concepts in core systems design
and implementation by applying them in assignments that often require
hands-on work with real computer systems.  {{site.course}} labs and
preparatory exercises familiarize you with relevant concepts and tools
at a small scale, but completing an assignment requires deeper
investment in learning, critical thinking, and the time for
both. Start early. Powering through assignments near the due date
rarely works well.

Real computer systems are often large and complicated, even if founded
on simple or elegant principles.  Likewise, {{site.course}}
assignments are structured, but typically require some independent
exploration, experimentation, and critical thinking to complete.
Mindlessly following exhaustive instructions is a job for computers.
The job of a computer scientist is to think critically and learn how
to construct or apply new ideas and tools.  The solutions themselves
are never as important as the insights you gain in the process of
finding solutions and understanding their context.  {{site.course}}
code assignments require you to read and reason about
specifications, apply concepts from class, learn something new beyond
what was covered in class, consult documentation, think critically
about design choices, synthesize plans for implementation, implement
your designs, evaluate or debug the results, and explain your
approach.  It might be messy; it is all important.

**Intellectual Independence and Self-Reliance:** A core [goal](#about) of
{{site.course}} is to develop independence, confidence, and
self-reliance as a learner, critical thinker, and computer scientist.
We [expect](#prep-expectations) you to take charge of your learning
and embrace an exploratory, self-reliant approach to experience the
reward of accomplishing large problem-solving tasks *under your own power*. In return,
enthusiasm, determination, careful preparation, and a *"can do, figure
it out"* attitude on your part are rewarded with [*enthusiastic*
support](#support-style) from us. The {{site.course}} staff are
always happy to help you *learn to find your own strategies to* solve
problems.

#### Tips From {{site.course}} Alums and Staff

Have more?  Let us know.

- **Embrace the [{{site.course}} perspective](#compact) and [approach
  to support](#support-style).**
- **Prepare before class.** It is fine if you do not understand all of
  the reading, video, or exercise, but seeing it early lets you ask
  better questions in class and hit the ground running in class
  exercises.
- **Do not worry about being wrong in class.** Students are often
  uncomfortable offering answers or suggestions in class for fear of
  peer judgment if they are wrong.  First of all, there are far fewer
  questions with a single right answer (or even a clear binary 
  right/wrong classification of answers) in this course than students expect.
  Second, we are learning!  "Being wrong" is normal and often more
  useful in class than "being right."  The best answer is a thoughtful
  answer.  We have more opportunity to learn if asking questions you
  cannot necessarily answer easily.  Better to share a "wrong" answer
  and explore the question's complexities while fixing the answer together
  than to keep that "wrong" answer to yourself for
  use on an exam.  We will make mistakes at least a few times in class
  (only occasionally on purpose).  Join us -- it's fun!  (Third, we do
  not tolerate students criticizing other students for being wrong in
  class.)
- **Take notes in class.** The slides can be useful, but they are just
  a prop.  The most useful and interesting insights happen in
  exercises and other discussion.  Writing down your solutions (and
  your approach!) is great reference for review later.
- **Ask questions** in class, in office hours, in tutor hours, in lab,
  of your classmates or on the email list (but first, read the
  [Honor Code definitions](#honor-code)).  The knowledge will not come
  to you.  Go get it.  We will often answer your questions with more
  questions.
- **Read the instructions** on assignments and understand the
  specifications.  There is often good advice about how to approach
  the problem most effectively.
- **Work incrementally.** As you work through an assignment (*after*
  you understand it), think and plan before you ever write any code or
  build any circuit.  Writing code before planning and writing one big
  pile of code before testing are two sure routes to disaster.  Plan
  carefully before you ever touch the keyboard.  Write
  pseudocode. Draw diagrams. When you are ready to write code, do it
  incrementally.  Write a small piece.  Write assertions to document
  your assumptions.  Test it.  Does it work?  No? Repeat until it
  does.  Now, *commit* it so you can get back to this point if things
  go wrong later.  Then, write the next small piece, and so on.  Step
  back occasionally and consider if you are still on track for your
  plan.  Does it need adjustment?
- **Read specifications carefully** and think critically about how
  best to implement them.
- **Consult documentation** to learn about relevant tools and library
  functions.
- **Experiment** to try out potential ideas.
- **Start assignments early and stay on schedule.** (Really, *early!*
  Really early. Ask any {{site.course}} alum.)  Before you can "just
  finish" the assignment, you will need time to absorb the task,
  understand how to approach it, plan, take a break when you get stuck
  (time away from a problem almost always helps), debug, ask
  questions, and more.  Some of the assignments are big or
  complicated. The checkpoints will help you keep on pace to finish
  comfortably.
- **Engage regularly on [{{site.forum.name}}](#forum).**
  Collaborating to ask and answer questions together helps students,
  tutors, and instructors help each other by pooling our collective
  curiosity, experience, knowledge, and critical thought. It also
  often leads to quicker response times than coming to drop-in hours.
- **Come to class and lab.** One or the other leads directly into the
  next assignment.  We do useful and fun things.  If you do not think
  so, then [let us know!](#feedback) (Oh, and lab counts toward your
  grade.)
- **Visit or work in the lab for [drop-in hours](#drop-ins) or other
  times.** Tackling challenging assignments next to your classmates
  builds camaraderie, reminds you that everyone has to work hard on
  these assignments, eases sharing of tips and techniques, and affords
  opportunities for collaborative silliness.
- **General tips from PLTC / Academic Peer Tutoring**:
  - [Text](https://www.wellesley.edu/pltc/handouts)
  - [Videos](https://www.youtube.com/playlist?list=PLFWQ1g8jNXn-jwRJkzOsuUE66nw5drfYa)


## Feedback

The instructors are always open to considering your comments,
suggestions, or concerns about the course or how we can make it more
accessible to you.  Please feel free to speak with us, send private
messages / email, etc.

{% comment %}
or
use the [anonymous feedback form]({{site.feedback}}) if you would
prefer.
{% endcomment %}


{:#reporting}
## Policies on Discrimination, Harassment, and Sexual Misconduct 

Wellesley College considers diversity essential to educational
excellence, and we are committed to being a community in which each
member thrives. The College does not allow discrimination or
harassment based on race, color, sex, gender identity or expression,
sexual orientation, ethnic or national origin or ancestry, physical or
mental disability, pregnancy or any other protected status under
applicable local, state or federal law.

If you or someone you know has experienced discrimination or
harassment, support is available to you:

- **Confidential** reporting: Students can report their experiences to
  [Health Services](https://www.wellesley.edu/healthservice)
  (781.283.2810); [Stone Center Counseling
  Service](https://www.wellesley.edu/counseling) (781.283.2839); or
  [Religious and Spiritual
  Life](https://www.wellesley.edu/campuslife/community/religiousandspirituallife)
  (781.283.2685). These offices are *not* required to report allegations
  of sexual misconduct to the College.
- **Non-confidential** reporting:
  - You can let either of your {{site.course}} instructors know. As
    faculty members, we *are* obligated to report allegations of
    sex-based discrimination to the [Nondiscrimination/Title IX
    Office](https://www.wellesley.edu/administration/offices/titleix).
  - You can report directly to the [Nondiscrimination/Title IX
    Office](https://www.wellesley.edu/administration/offices/titleix)
    (781.283.2451) to receive support, and to learn more about your
    options for a response by the College or about reporting to a
    different institution.
  - You can report to the [Wellesley College Police
    Department](https://www.wellesley.edu/police) (Emergency:
    781.283.5555, Non-emergency: 781.283.2121) if you believe a crime
    has been committed, or if there is an immediate safety risk.

---

You mean you read all the way down to *here*? Wow!! Watch out for [overflow]({{site.baseurl}}/topics/#integers).

