---
layout: page
title: Text Not Screenshots
short_title: "Use Text!"
---

Zulip tip of the day: **Use text, NOT screenshots.**

Text is **much** more useful than a screenshot when the goal is to share text. Text is transparent data: it can be searched, selected, edited, copied, zoomed, reflowed/reformatted, and more. Screenshots are opaque data that support none of those things. To preserve formatting of code or error messages, use [simple code formatting](https://zulip.com/help/format-your-message-using-markdown#code).

Example as text:

~~~
[Tue Feb 16 09:58:16]  ~/sandbox/screenshot-demo
bpw@tempest $ ./driver.pl 
1. Running './dlc -z' to identify coding rules violations.
/usr/include/stdc-predef.h:1: Warning: Non-includable file <command-line> included from includable file /usr/include/stdc-predef.h.
~~~

Same example as screenshot:

![Screenshot Demo](screenshot-demo.png)

Text examples should be [well-formatted](https://zulip.com/help/format-your-message-using-markdown#code) to distinguish them from the rest of a message. Here's what I typed/pasted in the Zulip window to achieve the [formatting](https://zulip.com/help/format-your-message-using-markdown#code) above:

    ~~~
    [Tue Feb 16 09:58:16]  ~/sandbox/screenshot-demo
    bpw@tempest $ ./driver.pl 
    1. Running './dlc -z' to identify coding rules violations.
    /usr/include/stdc-predef.h:1: Warning: Non-includable file <command-line> included from includable file /usr/include/stdc-predef.h.
    ~~~

If I want to get fancy, I can give a language for [syntax highlighting](https://zulip.com/help/format-your-message-using-markdown#code):

    ~~~ console
    [Tue Feb 16 09:58:16]  ~/sandbox/screenshot-demo
    bpw@tempest $ ./driver.pl 
    1. Running './dlc -z' to identify coding rules violations.
    /usr/include/stdc-predef.h:1: Warning: Non-includable file <command-line> included from includable file /usr/include/stdc-predef.h.
    ~~~

    ~~~ c
    int main(int argc, char** argv) {
        printf("Hello, world!\n");
        return 0;
    }
    ~~~

which produces this view:

~~~ console
[Tue Feb 16 09:58:16]  ~/sandbox/screenshot-demo
bpw@tempest $ ./driver.pl 
1. Running './dlc -z' to identify coding rules violations.
/usr/include/stdc-predef.h:1: Warning: Non-includable file <command-line> included from includable file /usr/include/stdc-predef.h.
~~~

~~~ c
int main(int argc, char** argv) {
    printf("Hello, world!\n");
    return 0;
}
~~~

Much more useful than a screenshot! 

Text is the appropriate and powerful choice for sharing text. If --- and only if --- you are attempting the rarer task of capturing a non-text element, such as a graphical user interface oddity, a screenshot is appropriate.
