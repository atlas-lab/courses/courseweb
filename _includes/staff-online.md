{% capture contact %}
<a href="{{ site.forum.url }}">{{site.forum.name}} questions</a>
/
<a href="{{ site.labspace.online.url }}">{{site.labspace.online.name}} drop-in hours</a>
{% endcapture %}

<table id="staff-table" class="course_calendar_block">
  <thead>
  <tr class="dark_row hide-narrow">
    <!--<th style="background: inherit; border: none;"></th>-->
    <th>Role</th>
    <th>Name</th>
    <th>Pronouns</th>
  </tr>
  </thead>

  <tbody>
  <tr class="light_row">
    <th>
      <a href="{{site.baseurl}}/about/#communication">Student</a>
    </th>
    <td>
      You + Classmates
    </td>
    <td>(ask)</td>
  </tr>
  {% for tutor in site.tutors %}
  <tr{% cycle ' class="dark_row"', ' class="light_row"' %}>
    <th><a href="{{site.baseurl}}/about/#communication">Tutor</a></th>
    <td>
      {{ tutor.name }}
    </td>
    <td>
    {{tutor.pronouns}}
    </td>
  </tr>
  {% endfor %}

  {% if site.si %}
  <tr id="si"{% cycle ' class="dark_row"', ' class="light_row"' %}>
    <th><a href="{{site.baseurl}}/about/#communication">SI Leader</a></th>
    <td>
      {{site.si.name}}
    </td>
    <td>
    {{site.si.pronouns}}
    </td>
  </tr>
  {% endif %}

  {% if site.labinstructor %}
  <tr id="{{site.labinstructor.first}}{{site.labinstructor.last}}"{% cycle ' class="dark_row"', ' class="light_row"' %}>
    <th><a href="{{site.baseurl}}/about/#communication">Lab Instructor</a></th>
    <td>
      <a href="{{site.labinstructor.url}}">{{site.labinstructor.first}} {{site.labinstructor.last}}</a><br />
      {{site.labinstructor.email}}
    </td>
    <td>
      {{site.labinstructor.pronouns}}
    </td>
  </tr>
  {% endif %}

  {% for instructor in site.instructors %}
  <tr id="{{instructor.first}}{{instructor.last}}"{% cycle ' class="light_row"', ' class="dark_row"' %}>
    <th><a href="{{site.baseurl}}/about/#communication">Instructor</a></th>
    <td>
      <a href="{{instructor.url}}">{{instructor.first}} {{instructor.last}}</a><br />
      {{instructor.email}}
    </td>
    <td>
      {{instructor.pronouns}}
    </td>
  </tr>
  {% endfor %}
  
</tbody>
</table>
