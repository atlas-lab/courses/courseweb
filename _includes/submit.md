{% if include.full %}
**Submit:** The course staff will collect your work directly from your hosted
repository.  To submit your work:

0. Test your source code files one last time. Make sure that, at a
   minimum, submitted source code is free of syntax errors and any
   other static errors (such as static type errors or name/scope
   errors). In other words: the code does not need to complete the
   correct computation when invoked, but it must be a valid program.
   We will not grade files that do not pass this bar.

0. Make sure you have
   **[committed]({{site.baseurl}}/docs/common/git/#commit)** your
   latest changes. (Replace `FILES` with the files you changed and
   `MESSAGE` with your commit message.)

   ~~~ console
   $ git add FILES
   $ git commit -m "MESSAGE"
   ~~~
1. **Run the command `{{site.repotool}} sign`** to sign your work and
   respond to any assignment survey questions.
   
   ~~~ console
   $ {{site.repotool}} sign
   ~~~
2. **[Push]({{site.baseurl}}/docs/common/git/#push)** your signature and your latest local commits to
   the hosted repository.
   
   ~~~ console
   $ git push
   ~~~

**Confirm:** All local changes have been submitted if the output of
[`git status`]({{site.baseurl}}/docs/common/git/#status) shows both:

- `Your branch is up to date with 'origin/{{site.default_branch}}'`, meaning all local
  commits have been pushed
- `nothing to commit`, meaning all local changes have been committed

**Resubmit:** If you realize you need to change something later, just repeat this
process.
{% else %}
**Submit your work** by [committing, signing, and
pushing]({{site.baseurl}}/docs/common/git/#submit) your latest work,
as summarized in the [assignment manifest](#page-title) and documented
in detail in the [Git
tutorial]({{site.baseurl}}/docs/common/git/#submit).
{% endif %}
