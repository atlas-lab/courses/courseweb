{% capture short %}

**You must use a [{{site.course}} GNU/Linux computing
environment]({{"/tools/#env" | relative_url}}) for CS 240 code
assignments.**

{% endcapture %}

{% capture longhead %}
CS 240 code assignments require a [{{site.course}} GNU/Linux computing environment]({{"/tools/#env" | relative_url}}).
{% endcapture %}

{% capture longbody %}

{{site.course}} code assignments are designed, tested, and graded in
the [{{site.course}} GNU/Linux computing environment]({{"/tools/#env" |
relative_url}}), since low-level system details matter in this course.
We guarantee support (help with provided tools) and consistency
(our grading and your testing use the same environment) only for work
in a [{{site.course}} GNU/Linux computing environment]({{"/tools/#env" | relative_url}}).

{% comment %}
and the
[{{site.appliance}} appliance]({{"/tools/#env" | relative_url}}).
{% endcomment %}

{% endcapture %}



{% if include.short %}

{% if include.aside %}
{% aside %}
{{ short }}
{% endaside %}
{% else %}
{% alert %}
{{ short }}
{% endalert %}
{% endif %}

{% else %}

{% if include.aside %}
{% aside {{longhead}} %}
{{ longbody }}
{% endaside %}
{% else %}
{% alert {{longhead}} %}
{{longbody}}
{% endalert %}
{% endif %}
{% endif %}
