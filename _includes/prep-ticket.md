{% alert Preparation is your ticket for assistance. %}

  - You must complete the [preparatory exercises](#{% if include.id %}{{include.id}}{% else %}prep{% endif %}) (and show evidence)
    before asking questions about code or debugging on the main assignment.
  - You may ask questions on [preparatory exercises](#{% if include.id %}{{include.id}}{% else %}prep{% endif %}) at any time.

{% endalert %}
