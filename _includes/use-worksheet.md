**Please write your answers on the {% include worksheet.md %} to streamline the grading process.**
Submit your work by [scanning a PDF of all worksheet pages (in order) and uploading it to Gradescope]({{site.baseurl}}/docs/common/gradescope/).
