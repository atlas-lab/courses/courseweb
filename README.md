---
toplevel: true
layout: page
title: README
---

- toc
{:toc}

# Course Web

This is a quick dump from an existing site.  Needs more documentation.

The directory `_demo` contains a working demo of how to use this material.

# Install

The website is generated using [Jekyll](http://jekyllrb.com).  To get
jekyll, assuming Ruby is installed (this may need `sudo`):

    $ gem install jekyll
    
You probably need Jekyl >= 3.8.  Tested on 3.8.3.

The top level of this repo is intended to  be placed as a top-level
subdirectory, `common`, of your Jekyll site directory. (A few references
to this name are hardcoded in this material.)

## Demo

The `_demo` directory contains such a site, plus a Makefile that
handles a few build tasks. To preview the `_demo` site before creating
your own:

    $ cd _demo
    $ make demo
    
Visit the URL shown in the output.

## Create your own site

To start a site from scratch:

1. Copy the `_demo` to a new `your-site` directory outside this repo.
2. Remove or replace the (now broken) symlink `your-site/README.md`, which
   displayed this `_README.md` as a page within the demo site.
3. Place this repo as `your-site/common`.
4. In `your-site`, `make serve` for incrementally updated viewing of your site.
5. Edit the demo resources in `your-site` to replace with your own.
6. Edit the `Makefile ` in `your-site` to change settings for remote deployment.
7. `make rsync` in `your-site` to publish remotely, or do your own thing.

# Directory Structure of a Course Site using Course Web

These parts are partly hardcoded in the (atrocious) `course.rb` plugin
logic, or not quite hardcoded but likely to get used as is.

* `/` refers to the top level of the course site material.
    * `_config.yml`: Top-level website configuration.  Base yours on the demo.
    * `_data`: structured data to support auto-generated calendar,
      assignment listings, assignment preambles, due dates, etc.
        * `assignments.yml`: list of assignments.  Keys match
          directories in `/assignements` and `/assignments`.
        * `holidays.yml`: holidays (no classes) in the semester, used
          to gray holidays on calendar.
        * `<instance>.yml`: master schedule, file name determined by the `instance` key in `_config.yml`
            - Controls auto-generated calendar, assignment listing,
            assignment preambles and due dates.
            - List of weeks, where each week is hash of day to
            list of events.
            - Events refer to:
                - Class meeting/reading `topic`s (by keys of `topics.yml`)
                - Assignment `assign`, `checkpoint`, and `due` dates (by
                  keys of `assignments.yml`)
                - `lab` topics (by keys of `labs.yml`.
                - `exam` events (see example)
        * `topics.yml`:    list of topics, assignments, labs
        * `visibility.yml`: list of assignments (keys of
          `assignments.yml`) that should appear linked (vs. simply
          listed) on the calendar page and the assignment list page.
          Assignment pages are always there; this simply determines
          whether links to them appear in auto-generated pages.
    * `_remote`: builds for remote deployment of site (`make build-remote`, `make rsync`)
    * `_site`: local builds of site
    * `assignments`: assignment spec pages
      * `index.md`: framing for auto-generated assignment listing.
      * one directory per key in
        `/_data/assignments.yml`.  Page for assignment `a` is
        `/assignments/a/index.md`.
    * `common`: this shared Jekyll course website infrastructure.
      Try not to put course-specific stuff here.
        - `css`: static CSS
        - `icons`: static icons
        - `_layouts`: Jekyll layouts
        - `_includes`: Jekyll includes
        - `_plugins`: custom Jekyll plugins
            - `course.rb`: all auto-generation of calendars,
              assignment info, etc.
            - ...
        - `_sass`: sources for CSS
    * `index.md`: front page, including auto-generated calendar
    * `Makefile`:
        - Before publishing, preview locally with `make serve` which
          provides live-updating <http://localhost:4000/~cs000/f18/>
        - `make rsync` to build and upload to public-facing web

Everything else in the demo is less essential/hardcoded and can be
changed/removed/added by editing pages alone.

* `/_data`:
  * `exams.yml`: list of exams -- currently just names, referenced
    by `<instance>.yml`, required if
  * `labs.yml`: list of labs -- currently just names, referenced
    by `<instance>.yml`
* `slides`: PDFs of slides.
  For each topic `t` in `/_data/topics.yml`:
    - Full PDF of non-hidden slides: `/slides/pdfs/t.pdf`
    - PDF of 4up handouts of select slides: `/slides/pdfs/t-handout.pdf`
* `syllabus/index.md`: syllabus
* `topics`: reading lists.
  * `/topics/_topics/t.md` gives free-form
    reading descriptions for key `t` of `/_data/topics.yml`.
    These are automatically listed in `/topics/index.md`.
  * `index.md`: reading/slide list per topic, in the order given by
    `/_data/<instance>.yml`, using the information from
    `/_topics` and slides and handouts in `/slides`.
    
    
# Formats

Most web-facing documents are written in
[Markdown](http://daringfireball.net/projects/markdown/basics) (file
extension `.md`) with occasional use of some
[Kramdown](http://kramdown.gettalong.org/quickref.html) extensions and
use of the ***atrocious***
[Liquid Template language that Jekyll uses](https://jekyllrb.com/docs/templates/). HTML
also works in these documents -- just leave out the html/head/body
tags.

Structured data is stored in [YAML](http://yaml.org/) files.

# Much More

Many little things not documented here.  Peruse the `_demo` and its
sources (including 3 full sample assignments, full schedule, etc.) to
find those.

# Caveats

If you add symlinks to outside of the web root while the Jekyll server is running
it might not notice them for incremental update.  Restart it.

# License

Course Web was <s>developed</s> built up in sedimentary accretions of time-constrained hacks for various courses by [Ben Wood](https://cs.wellesley.edu/~bpw/)

The code for Course Web is covered by `LICENSE.txt`.

Some materials in the `_demo` have their own self-contained licenses.
