#### Based on symlink_watcher.rb by Will Norris.  Original Copyright notice:

# Copyright 2014 Google. All rights reserved.
# Available under an MIT license that can be found in the LICENSE file.

#### Original LICENSE:

# Copyright 2002-2014 Will Norris <https://willnorris.com/>
#
# Unless noted otherwise, text content is licensed under a Creative Commons
# Attribution 4.0 License and code under an MIT License.
#
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# The symlink_watcher plugin extends jekyll-watch to also listen for changes in
# any symlinked sub-directories.

#### Revised 2018 by Benjamin P. Wood, Wellesley College
#
# - Replaced flat symlink search with recursive depth-first
#   enumeration of all directories, d, that are transitively reachable
#   from the source by a path including at least one symlink that
#   points directly to d, and not reachable from source by any path
#   through d's non-symbolic parent(s).

require 'find'
require 'jekyll-watch'

module Jekyll
  module Watcher

    # Depth-first search for all dirs reachable by symlink from under
    # watch_entrypoint.
    def find_roots!(watch_entrypoint, watch_roots)
      Find.find(watch_entrypoint).each do |path|
        if File.directory?(path) and File.symlink?(path)
          candidate = File.realpath(path)
          
          unless watch_roots.any? { |root| candidate.start_with?(root) }
            # Retain only those roots that are not descended from this directory.
            watch_roots.select! { |root| !root.start_with?(candidate) }
            # Add candidate as a root
            watch_roots << candidate
            # Search for additional links under this root.
            find_roots!(candidate, watch_roots)
          end
        end
      end
    end

    # Listen to all transitively reachable roots.
    def build_listener_with_symlinks(site, options = {})
      watch_roots = [File.realpath(options['source'])]
      find_roots!(options['source'], watch_roots)

      # https://github.com/guard/listen/wiki/Duplicate-directory-errors
      # Listen's ignore option does not really ignore directories for
      # the purposes of this error- all directories are still watched
      # by the native adapter, and events are only filtered after they
      # are fired (#274)
      
      require 'listen'
      # puts "Listening to " + watch_roots.to_s
      # puts "Ignoring " + listen_ignore_paths(options).to_s
      Listen.to(
        *watch_roots,
        :ignore => listen_ignore_paths(options),
        :force_polling => options['force_polling'],
        &(listen_handler(site))
      )
    end

    alias_method :build_listener_without_symlinks, :build_listener
    alias_method :build_listener, :build_listener_with_symlinks
  end
end
