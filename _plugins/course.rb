# Render the course calendar using _data:
#  - <instance>.yml
#  - holidays.yml
#  - topics.yml
#  - labs.yml
#  - exams.yml
#  - assignments.yml

# Yuck!  Sorry, it is a gradual accretion of many time-constrained
# quick and dirty extensions across multiple courses and several
# semesters, no time to clean up.


require 'date'

class Date
  def next_weekday()
    self + (if self.friday?
            3
           elsif self.saturday?
             2
           else
             1
            end)
  end
  def week_start()
    self - (self.wday % 7)
  end
end

module Schedule
  LDAY = %w{sun mon tue wed thu fri sat}
  DAYN = Hash[LDAY.map.with_index {|x,i| [x,i]}]
  LONG_DAY = {
    'sun' => "Sunday",
    'mon' => "Monday",
    'tue' => "Tuesday",
    'wed' => "Wednesday",
    'thu' => "Thursday",
    'fri' => "Friday",
    'sat' => "Saturday",
    'su' => "Sunday",
    'mo' => "Monday",
    'tu' => "Tuesday",
    'we' => "Wednesday",
    'th' => "Thursday",
    'fr' => "Friday",
    'sa' => "Saturday"
  }

  def date_string(date)
    date.strftime("%A %d %B").sub(" 0", " ")
  end

  def pred_weekstart(time)
    time.to_date().week_start() - 7
  end

  def this_weekstart(time)
    time.to_date().week_start()
  end

  WORK_TYPES = {
    'lec' => 'topic',
    'topic' => 'topic',
    'lab' => 'lab',
    'ex' => 'paper',
    'paper' => 'paper',
    'applied' => 'code',
    'code' => 'code',
    'exam' => 'exam',
  }

  def type_tag(s)
    type = WORK_TYPES[s]
    if type
      "<span class=\"tag #{type}_tag\">#{type}</span>"
    elsif s == "practice" or s.nil? or s.empty?
      ''
    else
      raise "Unknown type '#{s}'"
    end
  end

  def assignment_key(site)
    site["assignment_key"] || "assignments"
  end
  def assignments(site)
    assignment_list(site)
    site['assignments']
  end
  def assignment_list(site)
    if site['assignment_list']
      return site['assignment_list']
    end
    ps = {}
    week_base = site["start_date"].week_start
    assignments = site["data"][assignment_key(site)]
    site["data"][site["instance"]].each do |week|
      if week
        week.select {|k| DAYN.has_key?(k) }.each_pair do |day,items|
          date = week_base + DAYN[day]
          if items.class == Array
            items.each do |i|
              if i.class == Hash
                if i.size() > 1
                  raise "Invalid syntax.  At most one 'type: id' pair is allowed per entry in a day's list of items."
                end
                type,id = i.first
                case type
                when "topic", "lab", "exam", "paper", "alt", "altlab"
                when "assign"
                  if not assignments.include?(id)
                    raise "Invalid assignment id: " + id
                  end
                  ps[id] = [["assign", date]]
                when "preview"
                  if not assignments.include?(id)
                    raise "Invalid assignment id: " + id
                  end
                  ps[id] = [["assign", date]]
                else
                  begin
                    ps[id] << [type, date]
                  rescue
                    raise "Probably an assignment that is due but not (yet) assigned: id = #{id}, type = #{type}, date = #{date}"
                  end
                end
              end
            end # each item
          end # items plural
        end # each day
      end
      week_base += 7
    end # each week
    site['assignments'] = {}
    ps.each do |k,v|
      site['assignments'][k] = { "id" => k, "dates" => v }.merge(site["data"][assignment_key(site)][k])
    end
    site['assignment_list'] = site['assignments'].values.sort_by {|a| a["dates"].last.last}
    site['assignment_list']
  end
  
  def assignment_from_page(site, page)
    id = page["path"].split('/')[-2] #.last.sub(/\..*$/,"")
    assignments(site)[id]
  end

  def exam_from_page(site, page)
    id = page["path"].split('/').last.sub(/\..*$/,"")
    exams(site)[id]
  end
  
  def exams(site)
    exam_list(site)
    site['exams']
  end
  def exam_list(site)
    if site['exam_list']
      return site['exam_list']
    end
    ps = {}
    week_base = site["start_date"].week_start
    exams = site["data"]["exams"]
    site["data"][site["instance"]].each do |week|
      if week
        week.select {|k| DAYN.has_key?(k) }.each_pair do |day,items|
          date = week_base + DAYN[day]
          if items.class == Array
            items.each do |i|
              if i.class == Hash
                if i.size() > 1
                  raise "Invalid syntax.  At most one 'key: value' pair is allowed per entry in a day's list of items."
                end
                type,item = i.first
                if type == "exam"
                  id = item["id"]
                  if not exams.include?(id)
                    raise "Invalid exam id: " + id
                  end
                  item["date"] = date
                  if ps[id]
                    ps[id] << item
                  else
                    ps[id] = [item]
                  end
                end
              end
            end # each item
          end # items plural
        end # each day
      end
      week_base += 7
    end # each week
    site['exams'] = {}
    ps.each do |k,v|
      site['exams'][k] = { "id" => k, "dates" => v }.merge(site["data"]["exams"][k])
    end
    site['exam_list'] = site['exams'].values.sort_by {|a| a["dates"].last["data"]}
    site['exam_list']
  end


  def show(site, topics, labs, assignments, item)
    if item.class != Hash
      item
    else
      type,id = item.first
      if type == "lab" and site["data"]["labs"][id] and site["data"]["labs"][id]["link_topic"] and site["data"]["labs"][id]["topics"] and site["data"]["labs"][id]["topics"].first
        type = "topic"
        id = site["data"]["labs"][id]["topics"].first
      end
      case type
      when "alt", "altlab"
        id
      when "lab"
        "<a href=\"#{lab_url(site, id)}\">#{type_tag('lab')}</a> " + lab_link(site, id)
      when "paper"
        paper_link(site, id)
      when "topic"
        turl = topic_url(site, id)
        s = "<a href=\"#{turl}\">" + type_tag("topic") + "</a> "
        s += topic_link(site, id)
        topic = topics[id]
        unless topic
          raise "Invalid topic ID: #{id}"
        end
        # s += " <span class=\"topic_extras\">(#{topic_link(site, id, 'ref')}"
        # if handout_exists(site, id)
        #   s += ", " + handout_link(site, id, "handout")
        # end
        # if slide_exists(site, id)
        #   s += ", " + slide_link(site, id, "slides")
        # end
        # if slide_exists(site, id + "-4up")
        #   s += ", " + slide_link(site, id + "-4up", "4up")
        # end
        # s += ")</span>"
        s
      when "exam"
        desc = id
        event = if desc["event"]
                  desc["event"].capitalize + ": "
                else
                  ""
                end
        listing = event + site["data"]["exams"][desc["id"]]["title"]
        if site["data"]["visibility"] && site["data"]["visibility"].include?(desc["id"])
          "<a href=\"#{site["baseurl"]}/exams/#{desc["id"]}.html\">#{type_tag("exam")}</a> <a class=\"exam_label\" href=\"#{site["baseurl"]}/exams/#{desc["id"]}.html\">#{listing}</a>"
        else
          listing
        end
      else
        begin
          "<span>" + # id=\"#{type}-#{id}\">" +
            "<a href=\"#{assignment_url(site, id)}\">" +
            type_tag(assignments[id]['type']) +
            "</a>" +
            " <span class=\"assign_label\">#{type.capitalize}:</span> " +
            " <span class=\"assign_link\">#{assignment_link(site, assignments[id].merge({"id" => id}))}</span>" +
            "</span>"
        rescue
          raise "Invalid assignment ID: " + id
        end
      end
    end
  end

  def calendar_grid(site, src=nil)
    schedule = site["data"][src || site["instance"]]
    topics   = site["data"]["topics"]
    labs     = site["data"]["labs"]
    assignments = site["data"][assignment_key(site)]
    holidays = site["data"]["holidays"]

    day_abbrevs = schedule.reduce({}) do |acc,week|
      if week
        acc.merge(week)
      else
        acc
      end
    end.keys.filter {|k| DAYN.has_key?(k) }.sort_by {|k| DAYN[k]}
    light = true
    sunday = site["start_date"].week_start
    pred_day = sunday.prev_month

    "<table class=\"course_calendar_grid\"><thead><tr id=\"#{pred_weekstart(site['start_date']).to_s}\" class=\"col_headers dark_row\">" + day_abbrevs.map {|d| "<th>" + LONG_DAY[d] + "</th>"}.join("\n") + "</tr></thead><tbody>" + schedule.map do |week|
      lightdark = if light
                    "light"
                  else
                    "dark"
                  end
      # first_in_row = true
      row = "<tr class=\"#{lightdark}_row\" id=\"week-#{sunday}\">" + day_abbrevs.map do |day|
        date = sunday + DAYN[day]
        nmcl = if pred_day.mon != date.mon
                 " new_month"
               else
                 ""
               end
        pred_day = date
        sdate = "<span class=\"date_label\" id=\"#{date.to_s}\"><span class=\"date_label_dow\">#{date.strftime("%A")}</span> <span class=\"date_label_day\">#{date.strftime("%d").sub(/^0/,"")}</span> <span class=\"date_label_month#{nmcl}\">" + date.strftime("%b") + "</span></span>"
        # if first_in_row and not date.monday?
        #   sdate = "<span id=\"#{(sunday + DAYN["mon"]).to_s}\">" + sdate + "</span>"
        #   first_in_row = false
        # end

        if holidays.include?(date)
          hday = holidays[date]
          cls = case hday
                when /[Ee]xam/
                  "examperiod"
                when /[Rr]eading\s+[Pp]eriod/
                  "readingperiod"
                when /[Ss]chedule/
                  "altschedule"
                when /[Oo]ptional/
                  "altschedule"
                when /[Bb]egin/
                  "altschedule"
                else
                  "holiday"
                end
          "<td class=\"#{cls}#{nmcl}\">#{sdate} <span class=\"day_note\">#{hday}</span> "
        else
          "<td class=\"normalday#{nmcl}\">#{sdate} "
        end +
          if week && week.include?(day)
            if week[day].class == Array
              "<ul class=\"day_agenda\">" + week[day].map {|item| "<li>" + show(site, topics, labs, assignments, item).to_s + "</li>"}.join("\n") + "</ul>"
            else
              week[day].to_s
            end
          else
            ""
          end + "</td>"  
      end.join("\n") + "</tr>"
      sunday += 7
      # first_in_row = true
      light = !light
      row
    end.join("\n") + "</tbody></table>"
  end

  GROUP_KEY = "part"
  
  def calendar_block(site, by=GROUP_KEY, src=nil)
    schedule = site["data"][src || site["instance"]]
    topics   = site["data"]["topics"]
    labs     = site["data"]["labs"]
    assignments = site["data"][assignment_key(site)]
    holidays = site["data"]["holidays"]
    block_cols = site["schedule_columns"]

    day_abbrevs = if site["regular_days"]
                    site["regular_days"].sort_by {|k| DAYN[k]}
                  else
                    []
                  end
    light = true
    sunday = site["start_date"].week_start
    pred_day = sunday.prev_month

    block_count = 0

    block = nil
    week_color = "blue"

    "<table class=\"course_calendar_block\">\n<tbody>\n" + schedule.map.with_index do |week,weeki|
      week_block = ""

      week_days = (day_abbrevs + week.keys).uniq.select {|k| DAYN.has_key?(k) }.sort_by {|k| DAYN[k]}
      
      if week.has_key?(by)
        block_id = week[by]
        block = site["data"][by + "s"][block_id]
        week_color = block["color"]
        block_count += 1
        week_block += "<tr id=\"week-#{sunday}\" class=\"block_row\"><th id=\"block#{sunday}\" colspan=\"#{1 + block_cols.size}\" class=\"block_head #{week_color}\"><a href=\"#{site["baseurl"]}#{site["topicspath"]}#block-#{block_id}\">#{block["title"]}</a></th></tr>"
        week_block += "<tr class=\"label_row block_first_label_row\">\n"
      else
        week_block += "<tr id=\"week-#{sunday}\" class=\"label_row week_#{week_color}\">\n"
      end
      
      week_block += "<th class=\"block_col_header_date #{week_color}\"><a href=\"#week-#{sunday}\">Date</a></th>\n"
      block_cols.each do |col|
        week_block += "<th class=\"block_col_header_#{col["keys"].first} #{week_color}\"><a href=\"#{site["baseurl"]}#{col["path"]}\">#{col["title"]}</a></th>\n"
      end
      week_block += "</tr>\n"
      week_days.each do |day|
        lightdark = if light
                      "light"
                    else
                      "dark"
                    end
        date = sunday + DAYN[day]

        nmcl = if pred_day.mon != date.mon
                 " new_month"
               else
                 ""
               end
        pred_day = date
        sdate = "<span class=\"date_label\" id=\"#{date.to_s}\"><span class=\"date_label_dow\">#{date.strftime("%A")}</span> <span class=\"date_label_cal\"><span class=\"date_label_day\">#{date.strftime("%d").sub(/^0/,"")}</span> <span class=\"date_label_month#{nmcl}\">" + date.strftime("%B") + "</span></span></span>"

        holiday = holidays.include?(date)
        holiday_string = if holiday
                           "holiday "
                         else
                           ""
                         end
        week_block +=
          if holiday
            hday = holidays[date]
            cls = case hday
                  when /[Ee]xam/
                    "examperiod"
                  when /[Rr]eading/
                    "readingperiod"
                  when /[Ss]chedule/
                    "altschedule"
                  when /[Oo]ptional/
                    "altschedule"
                  when /[Bb]egin/
                    "altschedule"
                  else
                    "holiday"
                  end
            "<tr class=\"holiday #{lightdark}_row week_#{week_color} #{cls}\">\n" +
            "<td class=\"holiday #{nmcl}\">#{sdate} <span class=\"day_note\">#{hday}</span> </td>\n"
          else
            "<tr class=\"#{lightdark}_row week_#{week_color}\">\n" +
            "<td class=\"normalday#{nmcl}\">#{sdate}</td>\n"
          end

        block_cols.each do |col|
          empty = " empty"
          list = ""
          cell_label = if col["no_cell_label"]
                        ""
                      else
                        "<span class=\"cell_label\">#{col["title"]}: </span>"
                      end
          if week && week.include?(day) && week[day]
            items = week[day].select {|item| item.class == Hash && col["keys"].include?(item.first.first)}
            if !items.empty?
              empty = ""
              list = items.map do |item|
                type,id = item.first
                "<li>#{cell_label}" + show(site, topics, labs, assignments, item).to_s + "</li>\n"
              end.join()
              list = "<ul class=\"day_agenda\">\n#{list}</ul>\n"
            end
          end
          week_block += "<td class=\"#{holiday_string}block_col_date#{empty}\">\n\n#{list}</td>\n"
        end
        week_block += "</tr>\n"
        light = !light
      end
      sunday += 7
      week_block
    end.join("\n") + "</tbody>\n</table>\n"
  end

  def slides_exist(site, id)
    File.exists?("slides/#{id}.pdf")
  end
  alias_method :slide_exists, :slides_exist
  def slide_link(site, key, name=false)
    unless slide_exists(site, key)
      raise "Missing slides: " + key
    end
    link(site, "slides", key, name || key, ext="pdf")
  end
  alias_method :slides_link, :slide_link

  def handout_exists(site, id)
    slide_exists(site, id + "-handout")
  end
  def handout_link(site, id, name=nil)
    slide_link(site, id + "-handout", name)
  end

  def note_exists(site, id)
    File.exists?("notes/#{id}.md")
  end
  alias_method :notes_exist, :note_exists
  def note_link(site, key)
    unless note_exists(site, key)
      raise "Missing note: " + key
    end
    link(site, "notes", key, ext="html")
  end
  alias_method :notes_link, :note_link

  def code_exists(site, id)
    File.exists?("notes/#{id}")
  end
  alias_method :notes_exist, :note_exists
  def code_link(site, key)
    unless code_exists(site, key)
      raise "Missing code: " + key
    end
    link(site, "notes", key)
  end

  def topic_url(site, id)
    "#{site["baseurl"]}/topics/##{id}"
  end
  def topic_link(site, id, name=nil)
    begin
      topic = site["data"]["topics"][id]
      # if File.exists?("_topics/#{id}.md") or File.exists?("topics/_#{id}.md")  or topic["alias"]
      target = topic["alias"] || id
    rescue
      raise "Invalid topic ID: " + id
    end
    text = opt_marker(topic, name || topic["title"])
    url = topic_url(site, target)
    "<a class=\"topic_link\" href=\"#{url}\">#{text}</a>"
    # else
    #   topic["title"]
    # end
  end

  def paper_url(site, id)
    "#{site["baseurl"]}/papers/##{id}"
  end
  def paper_link(site, id, name=nil)
    begin
      paper = site["data"]["papers"][id]
      # if File.exists?("_topics/#{id}.md") or File.exists?("topics/_#{id}.md")  or topic["alias"]
      target = paper["alias"] || id
    rescue
      raise "Invalid paper ID: " + id
    end
    text = opt_marker(paper, name || paper["title"])
    url = paper_url(site, target)
    "<a class=\"topic_link\" href=\"#{url}\">#{text}</a>"
    # else
    #   topic["title"]
    # end
  end

  def opt_marker(item, text)
    if item["opt"]
      "<sup>+</sup>" + text
    else
      text
    end
  end


  def lab_url(site, id)
    "#{ site["baseurl"] }/#{ site["labpage"]}##{id}"
  end
  def lab_link(site, id, name=nil)
    begin
      lab = site["data"]["labs"][id]
    rescue
      raise "Invalid lab ID: " + id
    end
    text = opt_marker(lab, name || lab["title"])
    "<a class=\"topic_link\" href=\"#{lab_url(site, id)}\">#{text}</a>"
  end

  def link(site, section, key, name=key, ext=false)
    file = if ext
             key + "." + ext
           else
             key
           end
    "<a href=\"#{ site["baseurl"] }/#{section}/#{file}\">#{name}</a>"
  end

  def exam_url(site, id)
    site["data"]["exams"][id]["url"] || "#{site["baseurl"]}/exams/#{id}.html"
  end
  def exam_link(site, a)
    if a.nil?
      raise "nil exam given to exam_link"
    end
    id = if a.class == String
           a
         else
           a["id"]
         end
    a = site["data"]["exams"][id]
    if a.nil?
      raise "Invalid exam given to exam_link: #{id}"
    end
    title = a["title"]
    if site['data']['visibility'] && site["data"]["visibility"].include?(id)
      "<a class=\"topic_link\" href=\"#{exam_url(site, id)}\">#{title}</a>"
    else
      title
    end
  end

  def assignment_url(site, id, prefix="")
    site["data"][assignment_key(site)][id]["url"] || "#{site["baseurl"]}/#{prefix}#{assignment_key(site)}/#{id}/"
  end
  def assignment_link(site, a)
    if a.nil?
      raise "nil assignment given to assignment_link"
    end
    id = if a.class == String
           a
         else
           a["id"]
         end
    a = site["data"][assignment_key(site)][id]
    if a.nil?
      raise "Invalid assignment given to assignment_link: #{id}"
    end
    title = opt_marker(a, a["title"]) #.gsub(/ /,'&nbsp;')
    if site['data']['visibility'] && site["data"]["visibility"].include?(id)
      "<a class=\"topic_link\" href=\"#{assignment_url(site, id)}\">#{title}</a>"
    else
      title
    end
  end

  def assignment_dates(site, page)
    id = page["path"].split('/').last.sub(/\..*$/,"")
    sched = site["data"][site["instance"]]
    dates = []
    sunday = site["start_date"].week_start
    sched.each do |week|
      if week
        week.each do |day,agenda|
          if agenda
            agenda.each do |item|
              if item.class == Hash
                today = sunday + DAYN[day]
                case item.keys.first
                when "preview"
                  if item["assign"] == id
                    dates << ["preview", today]
                  end
                when "assign"
                  if item["assign"] == id
                    dates << ["assign", today]
                  end
                when "checkpoint"
                  if item["checkpoint"] == id
                    dates << ["checkpoint", today]
                  end
                when "due"
                  if item["due"] == id
                    dates << ["due", today]
                  end
                end
              end
            end
          end
        end
      end
      sunday += 7
    end
    dates
  end

  def paper_list(site)
    sched = site["data"][site["instance"]]
    papers = []
    sunday = site["start_date"].week_start
    sched.each do |week|
      if week
        week.select {|k| DAYN.has_key?(k) }.each do |day,agenda|
          if agenda
            agenda.each do |item|
              if item.class == Hash
                today = sunday + DAYN[day]
                if item.keys.first == "paper"
                  papers << { "id" => item["paper"], "paper" => site["data"]["papers"][item["paper"]], "date" => today }
                end
              end
            end
          end
        end
      end
      sunday += 7
    end
    papers
  end
  def topic_list(site)
    thing_list(site,"topic")
  end
  def lab_list(site)
    thing_list(site,"lab")
  end
  def thing_list(site,type)
    sched = site["data"][site["instance"]]
    dates = []
    sunday = site["start_date"].week_start
    sched.each do |week|
      if week
        week.select {|k| DAYN.has_key?(k) }.each do |day,agenda|
          if agenda
            agenda.each do |item|
              if item.class == Hash
                today = sunday + DAYN[day]
                if item.keys.first == type
                  if dates.last && dates.last[type] == item[type]
                    dates.last["dates"] << today
                  else
                    dates << item.merge({"dates" => [today]})
                  end
                end
              end
            end
          end
        end
      end
      sunday += 7
    end
    dates
  end

  def topic_labref_list(site)
    sched = site["data"][site["instance"]]
    topic2dates = {}
    topics = []
    sunday = site["start_date"].week_start
    sched.each do |week|
      if week
        week.select {|k| DAYN.has_key?(k) }.each do |day,agenda|
          if agenda
            agenda.each do |item|
              if item.class == Hash
                today = sunday + DAYN[day]
                case item.keys.first
                when "topic"
                  if !topic2dates.has_key?(item["topic"])
                    dates = []
                    topic2dates[item["topic"]] = dates
                    topics << item.merge({"dates" => dates})
                  end
                  topic2dates[item["topic"]] << {"context" => "topic", "date" => today}
                when "lab"
                  lab = site["data"]["labs"][item["lab"]]
                  if lab["topics"]
                    lab["topics"].each do |tid|
                      topic = site["data"]["topics"][tid]
                      # puts("#{item} => #{lab} => #{tid} => #{topic}")
                      if !topic2dates.has_key?(tid)
                        dates = []
                        topic2dates[tid] = dates
                        topics << {"topic" => tid, "dates" => dates}
                      end
                      topic2dates[tid] << {"context" => "lab", "date" => today}
                    end
                  end
                end
              end
            end
          end
        end
      end
      sunday += 7
    end
    topics
  end

  def lab_only(topic)
    topic["dates"].all? {|d| d["context"] == "lab"}
  end

  def topic_list_blocks(site)
    sched = site["data"][site["instance"]]
    sunday = site["start_date"].week_start
    block = nil
    blocks = []
    sched.each do |week|
      if week
        if week.has_key?(GROUP_KEY)
          dates = []
          block = {
            "title" => week[GROUP_KEY]["title"],
            "color" => week[GROUP_KEY]["color"],
            "id" => week[GROUP_KEY],
            "topics" => dates,
          }
          blocks << block
        end
        week.select {|k| DAYN.has_key?(k) }.each do |day,agenda|
          if agenda
            agenda.each do |item|
              if item.class == Hash
                today = sunday + DAYN[day]
                if item.keys.first == "topic"
                  if dates.last && dates.last["topic"] == item["topic"]
                    dates.last["dates"] << today
                  else
                    dates << item.merge({"dates" => [today]})
                  end
                end
              end
            end
          end
        end
      end
      sunday += 7
    end
    blocks
  end
  
  def exists(file)
    File.exists?(file)
  end
end

Liquid::Template.register_filter(Schedule)




