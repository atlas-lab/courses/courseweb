module SemanticsNotation
  class EvalTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
    end
    def render(context)
      "&darr;"
    end
  end
  class StepTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
    end
    def render(context)
      "&rarr;"
    end
  end

  Liquid::Template.register_tag('eval', SemanticsNotation::EvalTag)
  Liquid::Template.register_tag('step', SemanticsNotation::StepTag)
end
