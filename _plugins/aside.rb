# Asides and alerts
module Jekyll
  class AsideBoxTag < Liquid::Block
    def initialize(tag_name, heading, tokens)
      super
      @tagname = tag_name
      @heading = Liquid::Template.parse(heading)
    end
    def render(context)
      body = super(context)
      if body.strip.empty?
        puts "warning empty body"
      end
      %(

{::options parse_block_html="true" /}
<div class="#{@tagname}_wrapper">
<div class="box #{@tagname}_box">
<div class="warning">
#{@heading.render(context)}
</div>

#{body}

</div>
</div>
{::options parse_block_html="false" /}

)
    end
  end
  class RawAsideBoxTag < Liquid::Block
    def initialize(tag_name, heading, tokens)
      super
      @tagname = tag_name
      @heading = heading
    end
    def render(context)
      body = super(context)
      if body.strip.empty?
        puts "warning empty body"
      end
      %(

<div class="#{@tagname}_wrapper">
<div class="box #{@tagname}_box">
<div class="warning">
#{@heading}
</div>

#{body}

</div>
</div>

)
    end
  end
end

Liquid::Template.register_tag('alert', Jekyll::AsideBoxTag)
Liquid::Template.register_tag('aside', Jekyll::AsideBoxTag)
Liquid::Template.register_tag('rawalert', Jekyll::RawAsideBoxTag)
Liquid::Template.register_tag('rawaside', Jekyll::RawAsideBoxTag)


