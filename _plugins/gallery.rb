module Gallery
  def gallery(page)
    Dir.glob(File.join(File.dirname(page["path"]), "*.{png,gif,jpg}"))
      .map do |p|
      p = File.basename(p)
      "<img alt=\"#{p}\" title=\"#{p}\" src=\"#{p}\" />"
    end.join("")
  end
end

Liquid::Template.register_filter(Gallery)
