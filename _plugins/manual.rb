# Man page references.
module Jekyll
  class ManualRefLinkTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
      parts = text.strip.split(/[\(\)]/)
      @section = if parts.size > 1
                   parts[1]
                 else
                   "1"
                 end
      @name = parts[0]
    end
    def render(context)
      "<a href=\"http://linux.die.net/man/#{@section}/#{@name}\"><code>#{@name}(#{@section})</code></a>"
    end
  end
end

Liquid::Template.register_tag('man', Jekyll::ManualRefLinkTag)
Liquid::Template.register_tag('doc', Jekyll::ManualRefLinkTag)


